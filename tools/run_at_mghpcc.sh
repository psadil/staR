#!/bin/bash

module load singularity/singularity-2.4.5

#BSUB -n 44
#BSUB -R rusage[mem=1024] # ask for 1GB per job slot, or 36GB total
#BSUB -W 48:00
#BSUB -q long # which queue we want to run in
#BSUB -R "span[hosts=1]" # All job slots on the same node (needed for threaded applications)
#BSUB -J staHB

singularity exec staR.simg Rscript -e "staR::recovery_wf(jobs=7, n_chains=6)"
