data {
  int<lower=0> n; // number of observations
  int<lower=1> n_condition; // number of conditions (4)
  int<lower=1,upper=n_condition> condition[n];
  matrix[n, 2] y;
  vector[8] priors;
  int n_orders;
  matrix<lower=0, upper=1>[n,n_condition] X[n_orders, 2];
  int<lower=0, upper = 1>prior_only;  // should the likelihood be ignored?
}
transformed data{
  int D = 2;
  row_vector[n_orders] zeroes_order = rep_row_vector(0,n_orders);
}
parameters{
  cholesky_factor_corr[D] condition_omega[n_condition];
  matrix[n_condition-2, n_orders] zeta_raw[D]; // -1 for intercept, -1 for simplex identification
  ordered[2] condition_mu_raw[n_orders,D]; // first intercept, then old condition_mu_raw
  vector[n_orders-1] lambda_raw; // first value must be pinned for identifiability
  matrix<lower=0>[D, n_condition] trial_scale;
}
transformed parameters {
  simplex[n_orders] lambda = softmax(append_row(0,lambda_raw));
  matrix[n_orders, n] lps;
  real log_lik_real;
  matrix[n_condition, D] condition_mu_ordered[n_orders]; // condition effects on mean of latent
  matrix[n_condition-1, n_orders] zeta[D]; // proportion of effect in each dimension
  simplex[n_condition-1] softmax_zeta[D, n_orders];
  matrix[D, D] Sigma[n_condition];

  for(cond in 1:n_condition) Sigma[cond] = diag_pre_multiply(col(trial_scale,cond), condition_omega[cond]);

  for (d in 1:D){
		 zeta[d] = append_row(zeroes_order, zeta_raw[d]);
	}

  for(order in 1:n_orders){
    vector[n_condition-1] cumulative_sum_softmax_zeta[D];

    for(d in 1:D){
      softmax_zeta[d, order] = softmax( col(zeta[d], order));
			cumulative_sum_softmax_zeta[d] = cumulative_sum( softmax_zeta[d, order] );
		}

    condition_mu_ordered[order] = append_row(to_row_vector(condition_mu_raw[order, 1:2, 1]),
      append_col(condition_mu_raw[order, 1, 2] * cumulative_sum_softmax_zeta[1],
        condition_mu_raw[order, 2, 2] * cumulative_sum_softmax_zeta[2] ));

    for (i in 1:n){
      lps[order, i] = multi_normal_cholesky_lpdf(y[i,] |
        [X[order,1,i,] * col(condition_mu_ordered[order], 1), X[order,2,i,] * col(condition_mu_ordered[order], 2)],
        Sigma[condition[i]]);
    }
  }
  {
    vector[n_orders] tmp[n];
    for(i in 1:n) tmp[i] = col(lps, i);
    log_lik_real = log_mix(lambda, tmp);
    }
}
model {

  // priors
	for(d in 1:D){
		to_vector(zeta_raw[d]) ~ normal(0, priors[3]);
		for(order in 1:n_orders){
		  condition_mu_raw[order, d] ~ normal(0, priors[2]);
		}
	}
  to_vector(trial_scale) ~ gamma(priors[4],priors[5]);
  for(cond in 1:n_condition){
		condition_omega[cond] ~ lkj_corr_cholesky(priors[7]);
	}

  lambda_raw ~ normal(0, priors[8]);

  if (!prior_only) {
    target += log_lik_real;
  }
}
generated quantities {
	vector<lower=-1, upper=1>[n_condition] condition_rho;
  vector[n] log_lik;

  {
    matrix[n_orders, n] lps_tmp;
    matrix[D,D] Omega;
    vector[n_orders] lambda_logged = log_softmax(append_row(0,lambda_raw));

    for(cond in 1:n_condition){
      Omega = condition_omega[cond] * condition_omega[cond]';
  		condition_rho[cond] = Omega[1,2]; // correlation to assemble
  	}

    for(order in 1:n_orders){
      lps_tmp[order] = lps[order] + lambda_logged[order];
     }

    for (i in 1:n){
      if(prior_only && is_inf(sum(col(lps_tmp,i)))){
        log_lik[i] = negative_infinity();
      }else{
        log_lik[i] = log_sum_exp(col(lps_tmp,i));
      }
    }
  }
}

