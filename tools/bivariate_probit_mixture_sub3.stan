functions{
#include /functions/binormal.stan
}
data {
  int<lower=0> n; // number of observations
  int<lower=1> n_condition; // number of conditions (4)
  int<lower=1,upper=n_condition> condition[n];
  int<lower=1> n_subject; // The number of subjects
  int<lower=1,upper=n_subject> subject[n]; // Index indicating the subject for a current trial
  int<lower=1> n_item; // The number of subjects
  int<lower=1,upper=n_item> item[n]; // Index indicating the subject for a current trial
  matrix<lower = 0, upper=1>[n, 2] y;
  vector[8] priors;
  int n_orders;
  matrix<lower=0, upper=1>[n,n_condition] X[n_orders, 2];
  int<lower=0, upper = 1>prior_only;  // should the likelihood be ignored?
}
transformed data{
  int D = 2;
  row_vector[n_subject] zeroes_sub = rep_row_vector(0, n_subject);
}
parameters{
  corr_matrix[D] condition_omega[n_condition];
  matrix[n_condition-2, n_subject] zeta_raw[D, n_orders]; // proportion of monotonic effect
  matrix<lower=0>[D, n_orders] zeta_raw_scale; // population-level scale
  cholesky_factor_corr[D] zeta_raw_L[n_orders]; // population-level scale
  matrix[n_condition-2] zeta_raw_b[D, n_orders]; // population level base

  matrix[D, n_subject] beta_intercept_z[n_orders]; // base of monotonic effect
  matrix<lower=0>[D, n_subject] beta_raw_z[n_orders]; // magnitude of monotonic effect
  matrix[D, n_orders] beta_intercept_b; // population level base
  matrix<lower=0>[D, n_orders] beta_raw_b; // population level magnitude
  matrix<lower=0>[D, n_orders] beta_intercept_scale; // population-level scale
  matrix<lower=0>[D, n_orders] beta_raw_scale; // population-level scale
  cholesky_factor_corr[D] beta_intercept_L[n_orders];
  cholesky_factor_corr[D] beta_raw_L[n_orders];

  vector<lower=0>[n_orders-1] lambda_scale;
  vector[n_orders-1] lambda_location;
  cholesky_factor_corr[n_orders-1] lambda_L;
  matrix[n_orders-1, n_subject] lambda_raw;

  vector<lower=0>[D] item_scale;
  matrix[D, n_item] item_mu_raw;
  cholesky_factor_corr[D] item_L;
}
transformed parameters {
  matrix[n_orders, n_subject] lambda_logged = append_row(zeroes_sub, rep_matrix(lambda_location, n_subject) +
                                              (diag_pre_multiply(lambda_scale, lambda_L) * lambda_raw));
  matrix[n_item, D] item_mu = (diag_pre_multiply(item_scale, item_L) * item_mu_raw)';
  matrix[D, n_subject] beta_intercept[n_orders];
  matrix[D, n_subject] beta_raw[n_orders];
  vector[n] log_lik;
  matrix[n_condition, D] condition_mu_ordered[n_orders, n_subject];

	{
	  vector[n_condition-1] cumulative_sum_softmax_zeta[D];
	  matrix[n_orders, n_subject] lambda_logged_weird;
    vector[n_condition-1] softmax_zeta;
    matrix[n_condition-2, n_subject] zeta_raw_tmp = ;

	  for(s in 1:n_subject){
	    lambda_logged_weird[,s] = lambda_logged[,s] ./ n_item;
	  }

	  for(order in 1:n_orders){
	    beta_intercept[order] = rep_matrix(col(beta_intercept_b, order), n_subject) + (diag_pre_multiply(col(beta_intercept_scale,order), beta_intercept_L[order]) * beta_intercept_z[order]);
	    beta_raw[order] = rep_matrix(col(beta_raw_b, order), n_subject) + (diag_pre_multiply(col(beta_raw_scale,order), beta_raw_L[order]) * beta_raw_z[order]);

	  for(s in 1:n_subject){
	    for(d in 1:D){
	      softmax_zeta = softmax( append_row(0, col(zeta_raw[d,order], s)));
	      cumulative_sum_softmax_zeta[d] = cumulative_sum( softmax_zeta );
	    }

	    condition_mu_ordered[order,s] = append_row(to_row_vector(beta_intercept[order,,s]),
	    append_col(beta_raw[order,1, s] * cumulative_sum_softmax_zeta[1],
	    beta_raw[order,2, s] * cumulative_sum_softmax_zeta[2] ));
	    }
    }

    for (i in 1:n){
      vector[n_orders] lps = lambda_logged_weird[,subject[i]];
      for(order in 1:n_orders){
        lps[order] += biprobit_lpdf_real(y[i],
        item_mu[item[i],1] + X[order,1,i] * condition_mu_ordered[order,subject[i],,1],
        item_mu[item[i],2] + X[order,2,i] * condition_mu_ordered[order,subject[i],,2],
        condition_omega[condition[i],1,2]);
      }
      if(is_inf(sum(lps)) && prior_only){
        log_lik[i] = negative_infinity();
      }else{
        log_lik[i] = log_sum_exp(lps);
      }
    }
  }
}
model {

  // priors
	to_vector(beta_raw_b) ~ normal(0, priors[2]);
	to_vector(beta_intercept_b) ~ normal(0, priors[2]);
	to_vector(beta_intercept_scale) ~ gamma(priors[4], priors[5]);
	to_vector(beta_raw_scale) ~ gamma(priors[4], priors[5]);

	to_vector(zeta_raw_scale) ~ gamma(priors[4], priors[5]);

	for(order in 1:n_orders){
  	to_vector(beta_intercept_z[order]) ~ std_normal();
  	to_vector(beta_raw_z[order]) ~ std_normal();
  	beta_intercept_L[order] ~ lkj_corr_cholesky(priors[6]);
  	beta_raw_L[order] ~ lkj_corr_cholesky(priors[6]);
  	zeta_raw_L[order] ~ lkj_corr_cholesky(priors[6]);
		for(d in 1:D){
  		to_vector(zeta_raw[d,order]) ~ normal(0, priors[3]);
  	}
	}

  item_scale ~ gamma(priors[4], priors[5]);
  item_L ~ lkj_corr_cholesky(priors[6]);
  to_vector(item_mu_raw) ~ std_normal();

  for(cond in 1:n_condition){
		condition_omega[cond] ~ lkj_corr(priors[7]);
	}
	lambda_scale ~ gamma(priors[4], priors[8]);
	lambda_location ~ normal(0, priors[1]);
	lambda_L ~ lkj_corr_cholesky(priors[7]);
  to_vector(lambda_raw) ~ std_normal();

  if (!prior_only) {
    target += sum(log_lik);
  }
}
generated quantities {
  matrix[n_orders, n_subject] lambda = exp(lambda_logged);
  real<lower=-1.0, upper=1.0> item_rho; // correlation to assemble
	vector<lower=-1, upper=1>[n_condition] condition_rho;
	vector<lower=-1.0, upper=1.0>[n_orders] beta_intercept_rho; // correlation to assemble
	vector<lower=-1.0, upper=1.0>[n_orders] beta_raw_rho; // correlation to assemble

	for(cond in 1:n_condition){
		condition_rho[cond] = condition_omega[cond,1,2]; // correlation to assemble
	}
  {
    matrix[D, D] Sigma;
    Sigma = multiply_lower_tri_self_transpose(item_L);
    item_rho = Sigma[1,2];
    for(order in 1:n_orders){
    Sigma = multiply_lower_tri_self_transpose(beta_intercept_L[order]);
    beta_intercept_rho[order] = Sigma[1,2];
    Sigma = multiply_lower_tri_self_transpose(beta_raw_L[order]);
    beta_raw_rho[order] = Sigma[1,2];
    }
  }
}
