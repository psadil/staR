functions{
#include /functions/binormal.stan
}
data {
  int<lower=0> n; // number of observations
  int<lower=1> n_condition; // number of conditions (4)
  int<lower=1,upper=n_condition> condition[n];
  int<lower=1> n_subject; // The number of subjects
  int<lower=1,upper=n_subject> subject[n]; // Index indicating the subject for a current trial
  int<lower=1> n_item; // The number of subjects
  int<lower=1,upper=n_item> item[n]; // Index indicating the subject for a current trial
  matrix<lower = 0, upper=1>[n, 2] y;
  vector[8] priors;
  int n_orders;
  matrix<lower=0, upper=1>[n,n_condition] X[n_orders, 2];
  int<lower=0, upper = 1>prior_only;  // should the likelihood be ignored?
}
transformed data{
  int D = 2;
  row_vector[n_subject] zeroes_sub = rep_row_vector(0, n_subject);
}
parameters{
  cholesky_factor_corr[((n_condition-2)+2)*D] eta_L[n_orders]; // includes betas and zetas, in both tasks
  matrix<lower=0>[((n_condition-2)+2)*D, n_orders] eta_scale; // population-level scale
  matrix[((n_condition-2)+2)*D, n_orders] eta_b; // population level base
  matrix[((n_condition-2)+2)*D, n_subject] eta_z[n_orders]; // proportion of monotonic effect

  vector<lower=0>[n_orders-1] lambda_scale;
  vector[n_orders-1] lambda_location;
  cholesky_factor_corr[n_orders-1] lambda_L;
  matrix[n_orders-1, n_subject] lambda_raw;

  vector<lower=0>[D] item_scale;
  matrix[D, n_item] item_mu_raw;
  cholesky_factor_corr[D] item_L;
}
transformed parameters {
  matrix[n_orders, n_subject] lambda_logged = append_row(zeroes_sub, rep_matrix(lambda_location, n_subject) +
                                              (diag_pre_multiply(lambda_scale, lambda_L) * lambda_raw));
  matrix[n_item, D] item_mu = (diag_pre_multiply(item_scale, item_L) * item_mu_raw)';
  vector[n] log_lik;
  matrix[n_condition, D] condition_mu_ordered[n_orders, n_subject];

	{
	  vector[n_condition-1] cumulative_sum_softmax_zeta[D];
	  matrix[n_orders, n_subject] lambda_logged_weird;
    matrix[((n_condition-2)+2)*D, n_subject] eta;

	  for(s in 1:n_subject){
	    lambda_logged_weird[,s] = lambda_logged[,s] ./ n_item;
	  }

	  for(order in 1:n_orders){
	    eta = rep_matrix(col(eta_b, order), n_subject) + diag_pre_multiply(col(eta_scale,order), eta_L[order]) * eta_z[order];

	  for(s in 1:n_subject){
      cumulative_sum_softmax_zeta[1] = cumulative_sum( softmax( append_row(0, eta[1:2,s])) );
      cumulative_sum_softmax_zeta[2] = cumulative_sum( softmax( append_row(0, eta[3:4,s])) );

	    condition_mu_ordered[order,s] = append_row(to_row_vector(eta[5:6,s]),
	    append_col(eta[7, s] * cumulative_sum_softmax_zeta[1],
	    eta[8, s] * cumulative_sum_softmax_zeta[2] ));
	    }
    }

    for (i in 1:n){
      vector[n_orders] lps = lambda_logged_weird[,subject[i]];
      for(order in 1:n_orders){
        lps[order] += biprobit_lpdf_real(y[i],
        item_mu[item[i],1] + X[order,1,i] * condition_mu_ordered[order,subject[i],,1],
        item_mu[item[i],2] + X[order,2,i] * condition_mu_ordered[order,subject[i],,2],
        0);
      }
      if(is_inf(sum(lps)) && prior_only){
        log_lik[i] = negative_infinity();
      }else{
        log_lik[i] = log_sum_exp(lps);
      }
    }
  }
}
model {

  // priors
	to_vector(eta_b) ~ normal(0, priors[2]);
	to_vector(eta_scale) ~ gamma(priors[4], priors[5]);

	for(order in 1:n_orders){
  	to_vector(eta_z[order]) ~ std_normal();
  	eta_L[order] ~ lkj_corr_cholesky(priors[6]);
  }

  item_scale ~ gamma(priors[4], priors[5]);
  item_L ~ lkj_corr_cholesky(priors[6]);
  to_vector(item_mu_raw) ~ std_normal();

	lambda_scale ~ gamma(priors[4], priors[8]);
	lambda_location ~ normal(0, priors[1]);
	lambda_L ~ lkj_corr_cholesky(priors[7]);
  to_vector(lambda_raw) ~ std_normal();

  if (!prior_only) {
    target += sum(log_lik);
  }
}
generated quantities {
  matrix[n_orders, n_subject] lambda = exp(lambda_logged);
  real<lower=-1.0, upper=1.0> item_rho; // correlation to assemble
	matrix<lower=-1, upper=1>[n_condition*D, n_condition*D] condition_Sigma[n_orders];

  {
    matrix[D, D] Sigma;
    Sigma = multiply_lower_tri_self_transpose(item_L);
    item_rho = Sigma[1,2];
    for(order in 1:n_orders){
      condition_Sigma[order] = multiply_lower_tri_self_transpose(eta_L[order]);
    }
  }
}
