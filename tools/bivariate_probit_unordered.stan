functions{
#include /functions/binormal.stan
}
data {
  int<lower=0> n; // number of observations
  int<lower=1> n_condition; // number of conditions (4)
  int<lower=1> n_subject; // The number of subjects
  int<lower=1,upper=n_subject> subject[n]; // Index indicating the subject for a current trial
  int<lower=1> n_item; // The number of subjects
  int<lower=1,upper=n_item> item[n]; // Index indicating the subject for a current trial
  int<lower=1,upper=n_condition> condition[n];
  matrix<lower = 0, upper=1>[n, 2] y;
  vector[5] priors;
  int<lower=0, upper = 1> prior_only;  // should the likelihood be ignored?
}
transformed data{
  int D = 2;
}
parameters{
  corr_matrix[D] condition_omega[n_condition];
  matrix[n_condition, D] beta; // basically forced to be positive by data
  vector<lower=0.0>[D] subject_scale; // Variance of subject-level effects
  matrix[D, n_subject] subject_mu_raw; // Subject-level coefficients for the bivariate normal means
  cholesky_factor_corr[D] subject_L;
  vector<lower=0.0>[D] item_scale; // Variance of subject-level effects
  matrix[D, n_item] item_mu_raw; // Subject-level coefficients for the bivariate normal means
  cholesky_factor_corr[D] item_L;
}
transformed parameters {
  matrix[n_subject, D] subject_mu = (diag_pre_multiply(subject_scale, subject_L) * subject_mu_raw)';
  matrix[n_item, D] item_mu = (diag_pre_multiply(item_scale, item_L) * item_mu_raw)';
}
model {

  // priors
  to_vector(beta) ~ normal(0, priors[1]);

  subject_scale ~ gamma(priors[2], priors[3]);
  subject_L ~ lkj_corr_cholesky(priors[4]);
  to_vector(subject_mu_raw) ~ std_normal();
  item_scale ~ gamma(priors[2], priors[3]);
  item_L ~ lkj_corr_cholesky(priors[4]);
  to_vector(item_mu_raw) ~ std_normal();

  for(cond in 1:n_condition){
		condition_omega[cond] ~ lkj_corr(priors[5]);
	}

  if (!prior_only) {
    target += sum(biprobit_lpdf_vector(y,
      subject_mu[subject] + item_mu[item] + beta[condition],
      to_vector(condition_omega[condition,1,2]) ));
  }
}
generated quantities {
  real<lower=-1.0, upper=1.0> subject_rho; // correlation to assemble
  real<lower=-1.0, upper=1.0> item_rho; // correlation to assemble
	vector<lower=-1, upper=1>[n_condition] condition_rho;

	for(cond in 1:n_condition){
		condition_rho[cond] = condition_omega[cond,1,2]; // correlation to assemble
	}
  {
    matrix[D, D] Sigma;
    Sigma = multiply_lower_tri_self_transpose(subject_L);
    subject_rho = Sigma[1,2];
    Sigma = multiply_lower_tri_self_transpose(item_L);
    item_rho = Sigma[1,2];
  }

}
