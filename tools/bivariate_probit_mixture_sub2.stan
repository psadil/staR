functions{
#include /functions/binormal.stan
}
data {
  int<lower=0> n; // number of observations
  int<lower=1> n_condition; // number of conditions (4)
  int<lower=1,upper=n_condition> condition[n];
  int<lower=1> n_subject; // The number of subjects
  int<lower=1,upper=n_subject> subject[n]; // Index indicating the subject for a current trial
  int<lower=1> n_item; // The number of subjects
  int<lower=1,upper=n_item> item[n]; // Index indicating the subject for a current trial
  matrix<lower = 0, upper=1>[n, 2] y;
  vector[8] priors;
  matrix<lower=0, upper=1>[n,n_condition] X;
  int<lower=0, upper = 1>prior_only;  // should the likelihood be ignored?
}
transformed data{
  int D = 2;
}
parameters{
  corr_matrix[D] condition_omega[n_condition];
  matrix[n_condition-2, n_subject] zeta_raw[D];
  matrix[D, n_subject] beta_intercept_z;
  matrix[D, n_subject] beta_z[n_condition-1];
  vector[D] beta_intercept_b;
  matrix[D, n_condition-1] beta_b;
  cholesky_factor_corr[D] beta_intercept_L;
  cholesky_factor_corr[D] beta_L;
  vector<lower=0>[D] beta_intercept_scale;
  vector<lower=0>[D] beta_scale;

  vector<lower=0>[D] item_scale;
  matrix[D, n_item] item_mu_raw;
  cholesky_factor_corr[D] item_L;
}
transformed parameters {
  matrix[n_item, D] item_mu = (diag_pre_multiply(item_scale, item_L) * item_mu_raw)';
  matrix[D, n_subject] beta_intercept = rep_matrix(beta_intercept_b, n_subject) +
                   (diag_pre_multiply(beta_intercept_scale, beta_intercept_L) * beta_intercept_z);
  matrix[D, n_subject] beta[n_condition-1];
  matrix[n, D] mu;

  {
    matrix[D,D] scale_omega = diag_pre_multiply(beta_scale, beta_L);

    for(c in 1:(n_condition-1)){
      beta[c] = rep_matrix(col(beta_b, c), n_subject) + (scale_omega * beta_z[c]);
    }
  }

}
model {

  // priors
	to_vector(beta_b) ~ normal(0, priors[2]);
	beta_intercept_b ~ normal(0, priors[2]);
	to_vector(beta_intercept_scale) ~ gamma(priors[4], priors[5]);
	to_vector(beta_scale) ~ gamma(priors[4], priors[5]);

	to_vector(beta_intercept_z) ~ std_normal();
	beta_intercept_L ~ lkj_corr_cholesky(priors[6]);
	beta_L ~ lkj_corr_cholesky(priors[6]);

  item_scale ~ gamma(priors[4], priors[5]);
  item_L ~ lkj_corr_cholesky(priors[6]);
  to_vector(item_mu_raw) ~ std_normal();

  for(cond in 1:n_condition){
		condition_omega[cond] ~ lkj_corr(priors[7]);
	}
	for(c in 1:(n_condition-1)){
  	to_vector(beta_z[c]) ~ std_normal();
	}

  if (!prior_only) {
    target += sum(biprobit_lpdf_vector(y,mu, to_vector(condition_omega[condition,1,2])));
  }
}
generated quantities {
  real<lower=-1.0, upper=1.0> item_rho; // correlation to assemble
	vector<lower=-1, upper=1>[n_condition] condition_rho;
	real<lower=-1.0, upper=1.0> beta_intercept_rho; // correlation to assemble
	real<lower=-1.0, upper=1.0> beta_rho; // correlation to assemble

	for(cond in 1:n_condition){
		condition_rho[cond] = condition_omega[cond,1,2]; // correlation to assemble
	}
  {
    matrix[D, D] Sigma;
    Sigma = multiply_lower_tri_self_transpose(item_L);
    item_rho = Sigma[1,2];
    Sigma = multiply_lower_tri_self_transpose(beta_intercept_L);
    beta_intercept_rho = Sigma[1,2];
    Sigma = multiply_lower_tri_self_transpose(beta_L);
    beta_rho = Sigma[1,2];
  }
}
