#!/bin/bash

singularity exec staR.simg Rscript -e "staR::recovery_wf(jobs=100, n_chain=6)"
