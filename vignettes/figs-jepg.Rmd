---
title: "Vignette Title"
author: "Vignette Author"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Vignette Title}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
devtools::load_all()
library(tidyverse)
data("lateral", package = "datalateral")
library(afex)
library(emmeans)
```



```{r}

d <- lateral %>%
  dplyr::filter(!(list=="0")) %>%
  dplyr::select(-pair, -firstOfPair, -rt_afc, -rt_recall, -pas1, -pas2, -list, -responses) %>%
  dplyr::mutate(condition = as.numeric(condition),
                condition = factor(condition)) %>%
  rename(item = object) 

orig <- d %>% filter(expt == "original") %>%
  mutate(subject = fct_drop(subject),
         item = fct_drop(item))

replication <- d %>% filter(expt == "replication") %>%
  mutate(subject = fct_drop(subject),
         item = fct_drop(item))

# orig_afc_fit <- lme4::glmer(afc ~ condition + (1|subject) + (1|item), data = orig, family = binomial)
# orig_name_fit <- lme4::glmer(name ~ condition + (1|subject) + (1|item), data = orig, family = binomial)

# orig_afc_boot <- confint(orig_afc_fit, parallel = "multicore", ncpus=3, method = "boot")
# orig_name_boot <- lme4::glmer(name ~ condition + (1|subject) + (1|item), data = orig, family = binomial)

# orig_afc_post <- broom::tidy(orig_afc_fit, conf.int=TRUE, conf.method = "boot", effects="fixed")
# orig_name_post <- broom::tidy(orig_name_fit, conf.int=TRUE, conf.method = "boot", effects="fixed")

```

```{r}
cl <- parallel::makeCluster(rep("localhost", 2))

orig_afc_fit_a <- afex::mixed(afc ~ condition + (1|subject) + (1|item), 
                              data = orig, 
                              method = "PB",
                              args_test = list( nsim=1000, cl = cl))

orig_name_fit_a <- afex::mixed(name ~ condition + (1|subject) + (1|item), 
                               data = orig, 
                               method = "PB",
                               args_test = list( nsim=1000, cl = cl))

write_rds(orig_afc_fit_a,file.path(devtools::package_file(),"data-raw","orig_afc_fit_a.rds"))
write_rds(orig_name_fit_a,file.path(devtools::package_file(),"data-raw","orig_name_fit_a.rds"))

```

## Parametric Bootstrap

(p.values based on getting 0 / 1000 of reduced model with higher LogLik as compared to full (implies p=1/1001)

Calculated by

1) Generate 1000 samples from null model (intercept + item + subject only)
2) Calculate Likelihood ratio (2*[log(L) - log(L_0)] ) for each bootstrapped sample
3) P-value is then given by proportion of tail probabilities, with adjustment to avoid divide by 0 (p = [n_extreme + 1]/[1000+1])

Davison AC, Hinkley DV (1997). Bootstrap Methods and Their Application. Cambridge
University Press., Chapter 4

Ulrich Halekoh, Søren Højsgaard (2014)., A Kenward-Roger Approximation and Parametric Bootstrap Methods for Tests in Linear Mixed Models - The R Package pbkrtest., Journal of Statistical Software, 58(10), 1-30., http://www.jstatsoft.org/v59/i09/

> orig_afc_fit_a
Mixed Model Anova Table (Type 3 tests, PB-method)

Model: afc ~ condition + (1 | subject) + (1 | item)
Data: orig
     Effect df     Chisq p.value
1 condition  3 81.82 ***   .0010 
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘+’ 0.1 ‘ ’ 1
> orig_name_fit_a
Mixed Model Anova Table (Type 3 tests, PB-method)

Model: name ~ condition + (1 | subject) + (1 | item)
Data: orig
     Effect df      Chisq p.value
1 condition  3 374.27 ***   .0010
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘+’ 0.1 ‘ ’ 1

## KR Method

Fitting one lmer() model. [DONE]
Calculating p-values. [DONE]
Mixed Model Anova Table (Type 3 tests, KR-method)

Model: afc ~ condition + (1 | subject) + (1 | item)
Data: orig
     Effect         df         F p.value
1 condition 3, 5967.37 27.45 ***  <.0001
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘+’ 0.1 ‘ ’ 1


Fitting one lmer() model. [DONE]
Calculating p-values. [DONE]
Mixed Model Anova Table (Type 3 tests, KR-method)

Model: name ~ condition + (1 | subject) + (1 | item)
Data: orig
     Effect         df          F p.value
1 condition 3, 5966.50 128.68 ***  <.0001
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘+’ 0.1 ‘ ’ 1

```{r}
read_rds(file.path(devtools::package_file(),"data-raw","orig_afc_fit_a.rds"))
read_rds(file.path(devtools::package_file(),"data-raw","orig_name_fit_a.rds"))

emm_options(lmerTest.limit = 6200)
orig_afc_emm <- emmeans::emmeans(orig_afc_fit_a, "condition", lmer.df = "asym")
orig_name_emm <- emmeans::emmeans(orig_name_fit_a, "condition", lmer.df = "asym")
broom::tidy(pairs(orig_afc_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "tukey")
broom::tidy(pairs(orig_name_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "tukey")
```

> emmeans::emmeans(orig_afc_fit_a, "condition")
Note: D.f. calculations have been disabled because the number of observations exceeds 3000.
To enable adjustments, set emm_options(pbkrtest.limit = 6144) or larger,
but be warned that this may result in large computation time and memory use.
 condition    emmean         SE     df  lower.CL  upper.CL
 1         0.7625114 0.01950427 217.59 0.7240699 0.8009528
 2         0.7441229 0.01950427 217.59 0.7056814 0.7825643
 3         0.7928043 0.01950427 217.59 0.7543628 0.8312457
 4         0.8561605 0.01950427 217.59 0.8177190 0.8946020

Degrees-of-freedom method: satterthwaite 
Confidence level used: 0.95 

> emmeans::emmeans(orig_name_fit_a, "condition")
Note: D.f. calculations have been disabled because the number of observations exceeds 3000.
To enable adjustments, set emm_options(pbkrtest.limit = 6144) or larger,
but be warned that this may result in large computation time and memory use.
 condition    emmean         SE     df  lower.CL  upper.CL
 1         0.2881429 0.02936575 191.88 0.2302218 0.3460641
 2         0.4083355 0.02936575 191.88 0.3504144 0.4662567
 3         0.3791628 0.02936575 191.88 0.3212416 0.4370839
 4         0.5545671 0.02936575 191.88 0.4966460 0.6124882

Degrees-of-freedom method: satterthwaite 
Confidence level used: 0.95

> summary(pairs(orig_afc_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "tukey")
 contrast    estimate         SE      df    lower.CL    upper.CL t.ratio p.value
 1 - 2     0.01838850 0.01326051 5970.40 -0.01568777  0.05246478   1.387  0.5077
 1 - 3    -0.03029290 0.01325792 5969.89 -0.06436251  0.00377672  -2.285  0.1016
 1 - 4    -0.09364914 0.01326051 5970.40 -0.12772542 -0.05957287  -7.062  <.0001
 2 - 3    -0.04868140 0.01326051 5970.40 -0.08275768 -0.01460512  -3.671  0.0014
 2 - 4    -0.11203765 0.01325792 5969.89 -0.14610726 -0.07796804  -8.451  <.0001
 3 - 4    -0.06335625 0.01326051 5970.40 -0.09743253 -0.02927997  -4.778  <.0001

Confidence level used: 0.95 
Conf-level adjustment: tukey method for comparing a family of 4 estimates 
P value adjustment: tukey method for comparing a family of 4 estimates 

> summary(pairs(orig_name_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "tukey")
 contrast    estimate         SE      df    lower.CL    upper.CL t.ratio p.value
 1 - 2    -0.12019260 0.01378075 5969.51 -0.15560577 -0.08477944  -8.722  <.0001
 1 - 3    -0.09101982 0.01377787 5969.32 -0.12642557 -0.05561407  -6.606  <.0001
 1 - 4    -0.26642413 0.01378075 5969.51 -0.30183730 -0.23101097 -19.333  <.0001
 2 - 3     0.02917278 0.01378075 5969.51 -0.00624039  0.06458595   2.117  0.1478
 2 - 4    -0.14623153 0.01377787 5969.32 -0.18163728 -0.11082578 -10.614  <.0001
 3 - 4    -0.17540431 0.01378075 5969.51 -0.21081748 -0.13999115 -12.728  <.0001

Confidence level used: 0.95 
Conf-level adjustment: tukey method for comparing a family of 4 estimates 
P value adjustment: tukey method for comparing a family of 4 estimates 

## No adjustment (for only looking at Word - CFS)

> orig_afc_emm
 condition    emmean         SE  df asymp.LCL asymp.UCL
 1         0.7625114 0.01950427 Inf 0.7242837 0.8007390
 2         0.7441229 0.01950427 Inf 0.7058952 0.7823505
 3         0.7928043 0.01950427 Inf 0.7545766 0.8310319
 4         0.8561605 0.01950427 Inf 0.8179328 0.8943882

Degrees-of-freedom method: asymptotic 
Confidence level used: 0.95 

> summary(pairs(orig_afc_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "none")
 contrast    estimate         SE  df   asymp.LCL   asymp.UCL z.ratio p.value
 1 - 2     0.01838850 0.01326051 Inf -0.00760163  0.04437863   1.387  0.1655
 1 - 3    -0.03029290 0.01325792 Inf -0.05627794 -0.00430785  -2.285  0.0223
 1 - 4    -0.09364914 0.01326051 Inf -0.11963927 -0.06765901  -7.062  <.0001
 2 - 3    -0.04868140 0.01326051 Inf -0.07467153 -0.02269127  -3.671  0.0002 ## pt(3.671, Inf, lower.tail = FALSE) * 2
 2 - 4    -0.11203765 0.01325792 Inf -0.13802269 -0.08605260  -8.451  <.0001
 3 - 4    -0.06335625 0.01326051 Inf -0.08934638 -0.03736612  -4.778  <.0001

Confidence level used: 0.95 

> orig_name_emm
 condition    emmean         SE  df asymp.LCL asymp.UCL
 1         0.2881429 0.02936575 Inf 0.2305871 0.3456987
 2         0.4083355 0.02936575 Inf 0.3507797 0.4658914
 3         0.3791628 0.02936575 Inf 0.3216070 0.4367186
 4         0.5545671 0.02936575 Inf 0.4970113 0.6121229

Degrees-of-freedom method: asymptotic 
Confidence level used: 0.95

> summary(pairs(orig_name_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "none")
 contrast    estimate         SE  df  asymp.LCL   asymp.UCL z.ratio p.value
 1 - 2    -0.12019260 0.01378075 Inf -0.1472024 -0.09318282  -8.722  <.0001
 1 - 3    -0.09101982 0.01377787 Inf -0.1180240 -0.06401570  -6.606  <.0001
 1 - 4    -0.26642413 0.01378075 Inf -0.2934339 -0.23941435 -19.333  <.0001
 2 - 3     0.02917278 0.01378075 Inf  0.0021630  0.05618256   2.117  0.0343 ## pt(2.117, Inf, lower.tail = FALSE) * 2
 2 - 4    -0.14623153 0.01377787 Inf -0.1732357 -0.11922741 -10.614  <.0001
 3 - 4    -0.17540431 0.01378075 Inf -0.2024141 -0.14839453 -12.728  <.0001

Confidence level used: 0.95 

```{r}

orig_afc_fit <- lme4::lmer(afc ~ 0 + condition + (1|subject) + (1|item), data = orig, REML=FALSE, contrasts = list(condition="contr.sum"))
orig_name_fit <- lme4::lmer(name ~ 0 +condition + (1|subject) + (1|item), data = orig, REML=FALSE, contrasts = list(condition="contr.sum"))

orig_afc_ci <- confint(orig_afc_fit, method = "boot", nsim=1000, oldNames=FALSE, parm="beta_")
orig_name_ci <- confint(orig_name_fit, method = "boot", nsim=1000, oldNames=FALSE, parm="beta_")

orig_afc_fit_red <- lme4::lmer(afc ~ (1|subject) + (1|item), data = orig, REML=FALSE)
orig_name_fit_red <- lme4::lmer(name ~ (1|subject) + (1|item), data = orig, REML=FALSE)

orig_afc_pb_comp <- pbkrtest::PBmodcomp(orig_afc_fit, orig_afc_fit_red)
orig_name_pb_comp <- pbkrtest::PBmodcomp(orig_name_fit, orig_name_fit_red)


```


## Bootstrapped CI

> orig_afc_ci
                2.5 %    97.5 %
.sig01     0.13877374 0.1805263
.sig02     0.04927994 0.0833686
.sigma     0.36048487 0.3742023
condition1 0.72294231 0.7968732
condition2 0.70540016 0.7802311
condition3 0.75485383 0.8308820
condition4 0.81814917 0.8933724

> orig_name_ci
               2.5 %    97.5 %
condition1 0.2344114 0.3450065
condition2 0.3535543 0.4618832
condition3 0.3261288 0.4340857
condition4 0.5006586 0.6151452


```{r}

d <- lateral %>%
  dplyr::filter(!(list=="0")) %>%
  dplyr::select(-pair, -firstOfPair, -rt_afc, -rt_recall, -pas1, -pas2, -list, -responses) %>%
  dplyr::mutate(condition = as.numeric(condition),
                condition = factor(condition)) %>%
  rename(item = object) 

replication <- d %>% filter(expt == "replication") %>%
  mutate(subject = fct_drop(subject),
         item = fct_drop(item))

rep_afc_fit <- lme4::lmer(afc ~ 0 + condition + (1|subject) + (1|item), 
                          data = replication, REML=FALSE, contrasts = list(condition="contr.sum"))
rep_name_fit <- lme4::lmer(name ~ 0 + condition + (1|subject) + (1|item), 
                           data = replication, REML=FALSE, contrasts = list(condition="contr.sum"))

rep_afc_fit_red <- lme4::lmer(afc ~ (1|subject) + (1|item), data = replication, REML=FALSE)
rep_name_fit_red <- lme4::lmer(name ~ (1|subject) + (1|item), data = replication, REML=FALSE)

rep_afc_pb_comp <- pbkrtest::PBmodcomp(rep_afc_fit, rep_afc_fit_red)
rep_name_pb_comp <- pbkrtest::PBmodcomp(rep_name_fit, rep_name_fit_red)

rep_afc_emm <- emmeans::emmeans(rep_afc_fit, "condition", lmer.df = "asym")
rep_name_emm <- emmeans::emmeans(rep_name_fit, "condition", lmer.df = "asym")
summary(pairs(rep_afc_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "none")
summary(pairs(rep_name_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "none")


```



```{r}

d <- lateral %>%
  dplyr::filter(!(list=="0")) %>%
  dplyr::select(-pair, -firstOfPair, -rt_afc, -rt_recall, -pas1, -pas2, -list, -responses) %>%
  dplyr::mutate(condition = as.numeric(condition),
                condition = factor(condition)) %>%
  rename(item = object) 

replication <- d %>% filter(expt == "replication") %>%
  mutate(subject = fct_drop(subject),
         item = fct_drop(item)) %>%
  gather(question, response, afc, name) %>%
  mutate(question = factor(question))

rep_fit <- lme4::lmer(response ~ 0 + question*condition + (0+question|subject) + (0+question|item), 
                      data = replication, REML=FALSE, contrasts = list(condition="contr.sum"))

rep_fit_red <- lme4::lmer(response ~ 0+question + (question||subject) + (question||item), 
                          data = replication, REML=FALSE)

rep_pb_comp <- pbkrtest::PBmodcomp(rep_fit, rep_fit_red)

rep_emm <- emmeans::emmeans(rep_fit, pairwise ~ condition|question, lmer.df = "asym")
summary(pairs(rep_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "none")


```


# GLMER


## Replication

```{r}
replication <- d %>% filter(expt == "replication") %>%
  mutate(subject = fct_drop(subject),
         item = fct_drop(item)) %>%
  gather(question, response, afc, name) %>%
  mutate(question = factor(question))

rep_fit <- lme4::lmer(response ~ 0 + condition:question + (0 +question|subject) + (0 +question|item),
                       data = replication, 
                       REML=FALSE, 
                       contrasts = list(condition="contr.sum"))

rep_emm <- emmeans::emmeans(orig_fit, ~condition|question, lmer.df = "asym")
summary(pairs(rep_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "none")
contrast(rep_emm, interaction="pairwise", by=NULL)

rep_red <- lme4::lmer(response ~ 0 + question + (0+question|subject) + (0+question|item),
                       data = replication, 
                       REML=FALSE)

rep_pb_comp <- pbkrtest::PBmodcomp(rep_fit, rep_red)
```

```{r}
rep_pb_comp
```


```{r}
summary(pairs(rep_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "none")
```


```{r}
contrast(rep_emm, interaction="pairwise", by=NULL)
```



```{r}

orig_fit_bn <- lme4::glmer(response ~ condition:question + (question|subject) + (question|item),
                           data = orig, 
                           contrasts = list(condition="contr.sum"),
                           family = binomial)

orig_bn_emm <- emmeans::emmeans(orig_fit_bn, "condition", by = "question", type = "response")
summary(pairs(orig_bn_emm), infer = c(TRUE,TRUE), level=.95,  adjust = "tukey")

orig_bn_red <- lme4::glmer(response ~ question + (question|subject) + (question|item),
                           data = orig, 
                           family=binomial)

orig_pb_comp <- pbkrtest::PBmodcomp(orig_fit_bn, orig_bn_red)

```

