
#' construct filter of data based on pas rating
#'
#' @param d dataset to filter
#'
#' @return minimum pas ratings
#' @export
calc_min_pas <- function(d){
  out <- d %>%
    dplyr::filter(condition == "cfs" & list > 0) %>%
    dplyr::select(subject, list, object, pas1, pas2) %>%
    dplyr::mutate(pas1 = as.numeric(as.character(pas1)),
           pas2 = as.numeric(as.character(pas2))) %>%
    dplyr::mutate(min_pas = purrr::map2_dbl(pas1, pas2, min, na.rm=TRUE))
  return(out)
}

#' apply filter to dataset
#'
#' @param d dataset to filter
#' @param low_pass_d output of calc_min_pas(d)
#' @param cutoff numeric value, objects that are below which will be excluded
#'
#' @return filtered dataset
#' @export
apply_low_pas <- function(d, low_pas_d, cutoff = 3){
  out <- d %>%
    dplyr::filter(list > 0) %>%
    dplyr::select(-pas1, -pas2) %>%
    dplyr::left_join(low_pas_d, by = c("subject","list","object")) %>%
    dplyr::filter(!(condition == "cfs") | (condition == "cfs" & (min_pas < cutoff)))
  return(out)
}

#' Calculate and apply pas filter to dataset
#'
#' @param d dataset to filter
#' @param cutoff only allow pas rating below this value
#'
#' @return filtered dataset
#' @export
filter_on_pas <- function(d, cutoff = 3){
  out <- apply_low_pas(d, calc_min_pas(d), cutoff = cutoff)
  return(out)
}

