#' run the model
#'
#' most parameters passed on to rstan::sampling
#'
#' @param stan_data from compose_standata
#' @param model model from this package to sample from
#' @param ... see rstan::sampling
#'
#' @export
run_stan <- function(stan_data,
                     model = "bivariate_probit_mixture",
                     init_r = 0.25,
                     ...) {

  post <- rstan::sampling(
    object = stanmodels[model][[1]],
    data = stan_data,
    init_r = init_r, # need tight initialization for moving around probits
    ...
  )

  return(post)
}


