#' drake workflow for running model recovery simulations
#'
#' @param n_chain integery, number of chains of each model to run
#' @param condition_rho,subject_rho numeric (vector) of different levels of correlation to consider
#' @param n_item,n_subject integers. n_item (vector) is per condition, n_subject is overall
#' @param condition_loc make list
#' @param priors see compose_data
#' @param item_scale,subject_scale numeric (vector), group-level variability in the simulation of effets
#' @param tau numeric (vector) scale of trial-level variability. will control effect size (as determined by cmr)
#' @param iter,warmup,cores,pars,include,control,chains see rstan::sampling
#' @param n_expt, integer. number of repeated datasets to create
#' @param data_type,model_type,data_style character vectors of data and models to create or fit (one of monotonic or nonmonotonic)
#'
#' @return creates workflow ready for drake::make
#' @export
#'
#' @examples
#' \dontrun{
#' wf_plan <- recovery_wf(n_subject = 20, n_chain=2, n_expt=2, tau = 1)
#' wf_plan <- recovery_wf(n_subject = 20, n_chain=2, n_expt=2, tau=c(2,.8,1), control = list(adapt_delta = 0.99))
#' wf <- recovery_wf(n_expt=2, n_chain=2,
#'                  condition_rho = c(-.9,0,.5),
#'                  subject_rho = c(-.5,0,.5),
#'                  n_subject = 50, n_item = 40,
#'                  condition_loc = list(list(c(-1,-.5,.5,1),c(-1,.5,-.5,1))),
#'                  data_style = "binary",
#'                  model = "bivariate_probit_mixture",
#'                  subject_scale=1,
#'                  item_scale=1
#'                  )
#' drake::make(wf_plan)
#' # total fits will be 2 * 2 * 3 * 2 * 2 (first extra 2 for running both monotonic and nonmonotonic data, second for each of those models)
#' }
recovery_wf <- function(model,
                        n_expt = 1,
                        n_chain = 1,
                        condition_rho = 0,
                        subject_rho = 0,
                        n_item = 1,
                        n_subject = 1,
                        item_scale = 0.25,
                        tau = 1,
                        subject_scale = 0.25,
                        iter = 1500,
                        warmup = 1000,
                        cores = 1,
                        chains = 1,
                        pars = c("item_mu_raw","subject_mu_raw", "condition_omega",
                                 "lps","subject_L","item_L","lambda_logged",
                                 "log_lik_real","lambda_raw"),
                        include = FALSE,
                        control = list(adapt_delta=.9),
                        data_type = "nonmonotonic",
                        data_style = "continuous",
                        model_type = c("monotonic","encompassing","nonmonotonic"),
                        priors = c(0.5, 0.5, 1.5, 2, 4, 1.5, 1.5, 1.5),
                        condition_loc = NULL){

  wf_data <- drake::drake_plan(data = gen_dataset_cart(n_item = N_ITEM,
                                                       n_subject = N_SUBJECT,
                                                       condition_rho = CONDITION__RHO,
                                                       subject_rho = SUBJECT_RHO,
                                                       item_scale = ITEM__SCALE,
                                                       tau = TAU__,
                                                       subject_scale = SUBJECT__SCALE,
                                                       type = "TYPE",
                                                       condition_loc = LOC__),
                               strings_in_dots = "literals") %>%
    drake::evaluate_plan(., rules = list(CONDITION__RHO = condition_rho,
                                         SUBJECT_RHO = subject_rho,
                                         ITEM__SCALE = item_scale,
                                         TAU__ = tau,
                                         SUBJECT__SCALE = subject_scale,
                                         N_ITEM = n_item,
                                         N_SUBJECT = n_subject,
                                         TYPE = data_type,
                                         LOC__ = condition_loc
    )) %>%
    drake::expand_plan(values = stringr::str_c("expt", 1:n_expt))

  wf_standata <- drake::drake_plan(standata = make_standata_simul(d=dataset__,
                                                                  model_type = "MODEL_TYPE",
                                                                  data_style = "DATA_STYLE",
                                                                  priors = !!priors),
                                   strings_in_dots = "literals") %>%
    drake::evaluate_plan(., rules = list(dataset__ = wf_data$target,
                                         MODEL_TYPE = model_type,
                                         DATA_STYLE = data_style
    ))

  wf_runstan <- drake::drake_plan(fit = run_stan(stan_data = dataset__,
                                                 model = !!model,
                                                 iter = !!iter,
                                                 warmup = !!warmup,
                                                 cores = !!cores,
                                                 chains = !!chains,
                                                 pars = !!pars,
                                                 include = !!include,
                                                 control = !!control,
                                                 chain_id = CHAIN)) %>%
    drake::evaluate_plan(rules = list(dataset__ = wf_standata$target,
                                      CHAIN = 1:n_chain),
                         trace=TRUE)

  wf_gather_chains <- drake::gather_by(wf_runstan, dataset__) %>%
    dplyr::select(-dataset__, -dataset___from, -CHAIN, -CHAIN_from)

  tmp <- wf_gather_chains %>% dplyr::filter(stringr::str_detect(target, "^target"))

  wf_stanfit <- drake::drake_plan(stanfit = rstan::sflist2stanfit(sflist = dataset__)) %>%
    drake::plan_analyses(datasets = tmp)

  wf_ic <- drake::drake_plan(ic = .calc_ic(fits = dataset__)) %>%
    drake::plan_analyses(dataset = wf_stanfit)

  wf_gather_ic <- drake::gather_plan(wf_ic, gather="rbind", target = "ics")

  wf_plan <- rbind(wf_data, wf_standata, wf_gather_chains, wf_stanfit, wf_ic, wf_gather_ic)

  return(wf_plan)

}

#' calculate ic values during recovery_wf
#'
#' @param fits output from rocovery_wf
#'
#' @return tbl with need calcs
#' @export
.calc_ic <- function(fits){

  ll <- loo::extract_log_lik(fits)
  w <- loo::waic(ll)
  l <- rstan::loo(fits)

  d <- tibble::tibble(waic = list(w),
                      loo = list(l))

  return(d)
}


#' gather ic values into single tibble
#'
#' @param ics output from rocovery_wf
#'
#' @return tbl with need calcs
#' @export
.gather_ic <- function(ics){

  ic <- tibble::tibble(id = names(ics)) %>%
    dplyr::mutate(waic = map(id, ~ics[.x][[1]]$waic[[1]]),
                  loo = map(id, ~ics[.x][[1]]$loo[[1]]))

  return(ic)
}


.add_eff <- function(ics, value = "waic", ses = 2){


  if(value == "waic"){
    out <- ics %>%
      dplyr::mutate(model = stringr::str_extract(id, "_monotonic|_encompassing"),
                    model = stringr::str_extract(model, "[[:alpha:]]+")) %>%
      dplyr::mutate(id = str_extract(id, "_data.*nonmonotonic") %>% str_extract("data.*"),
                    id = paste0(id, "_expt", expt),
                    data = purrr::map(id, drake::readd, character_only=TRUE),
                    omega = purrr::map_dbl(data, estimate_true_effect),
                    condition_rho = purrr::map_dbl(data, ~unique(.x$condition_rho))) %>%
      dplyr::select(-loo, -id) %>%
      tidyr::spread(model, waic) %>%
      dplyr::mutate(comparison = purrr::map2(monotonic, encompassing, loo::compare),
                    elpd_diff = purrr::map_dbl(comparison, ~purrr::pluck(.x, 1)),
                    se = purrr::map_dbl(comparison, ~purrr::pluck(.x, 2)),
                    rejectnull = elpd_diff > ses*se) %>%
      dplyr::select(-encompassing, -monotonic, -comparison, -data)
  }

  return(out)
}
