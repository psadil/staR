
#' Initialize datalateral workflow
#'
#' @param d dataset to analyze
#' @param model,n_chain,model_type,priors see recovery_wf
#' @param iter,warmup,cores,pars,include,control,chains see rstan::sampling
#'
#' @return workflow for running with drake
#' @export
init_datalateral_nomix_wf <- function(d,
                                      experiment,
                                      model,
                                      n_chain = 1,
                                      iter = 1500,
                                      warmup = 1000,
                                      cores = 1,
                                      chains = 1,
                                      pars = c("item_mu_raw","subject_mu_raw",
                                               "condition_omega","lps","subject_L",
                                               "item_L","lambda_logged","log_lik_real",
                                               "lambda_raw","beta_intercept_L","beta_raw_L"),
                                      include = FALSE,
                                      control = list(adapt_delta=.9),
                                      model_type = c("first","second","third","fourth"),
                                      priors = c(2, 0.5, 1, 2, 4, 1.5, 1.5, 1)) {

  tmp <- d %>%
    dplyr::filter(!(expt == "buggy")) %>%
    dplyr::mutate(subject = forcats::fct_drop(subject))

  wf_standata <- drake::drake_plan(standata = make_standata_simul_nomix(d = !!tmp,
                                                                        experiment = "EXPT",
                                                                        model_type = "MODEL_TYPE",
                                                                        data_style = "binary",
                                                                        priors = !!priors),
                                   strings_in_dots = "literals") %>%
    drake::evaluate_plan(., rules = list(MODEL_TYPE = model_type,
                                         EXPT = experiment))

  wf_runstan <- drake::drake_plan(fit = run_stan(stan_data = dataset__,
                                                 model = "MODEL",
                                                 iter = !!iter,
                                                 warmup = !!warmup,
                                                 cores = !!cores,
                                                 chains = !!chains,
                                                 pars = !!pars,
                                                 include = !!include,
                                                 control = !!control,
                                                 chain_id = CHAIN),
                                  strings_in_dots = "literals") %>%
    drake::evaluate_plan(rules = list(dataset__ = wf_standata$target,
                                      CHAIN = 1:n_chain,
                                      MODEL = model),
                         trace=TRUE)


  wf_gather_chains <- drake::gather_by(wf_runstan, dataset__, MODEL) %>%
    dplyr::select(-dataset__, -dataset___from, -CHAIN, -CHAIN_from, -MODEL, -MODEL_from)

  tmp <- wf_gather_chains %>% dplyr::filter(stringr::str_detect(target, "^target"))

  wf_stanfit <- drake::drake_plan(stanfit = rstan::sflist2stanfit(sflist = dataset__)) %>%
    drake::plan_analyses(datasets = tmp)

  wf_ic <- drake::drake_plan(ic = .calc_ic(fits = dataset__)) %>%
    drake::plan_analyses(dataset = wf_stanfit)

  wf_gather_ic <- drake::gather_plan(wf_ic, gather="list", target = "ics")
  wf_tbl_ic <- drake::drake_plan(ic_tbl = .gather_ic(dataset__)) %>%
    drake::plan_analyses(dataset = wf_gather_ic)

  wf_plan <- rbind(wf_standata, wf_gather_chains, wf_stanfit, wf_ic, wf_gather_ic, wf_tbl_ic)


  return(wf_plan)

}
