vector softmax_id(vector alpha){
  vector[num_elements(alpha) + 1] alphac1;
  for (k in 1:num_elements(alpha)) alphac1[k] = alpha[k];
  alphac1[num_elements(alpha)] = 0;
  return softmax(alphac1);
}

vector log_softmax_id(vector alpha){
  vector[num_elements(alpha) + 1] alphac1;
  for (k in 1:num_elements(alpha)) alphac1[k] = alpha[k];
  alphac1[num_elements(alpha)] = 0;
  return log_softmax(alphac1);
}
