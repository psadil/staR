row_vector binormal_cdf_vector(matrix z, vector rho) {
  // NOTE: this funciton has a hard time computing values very close to 0.
  // In those cases, it sometimes incorrectly outputs a negative number.
  // which is forced to 0. Also, Phi_approx is used as it has better chance of
  // avoiding underflow.
  // expressions come from https://github.com/stan-dev/stan/issues/2356
  // and https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2924071
  int n = num_elements(rho);
  vector[n] denom = sqrt((1.0 + rho) .* (1.0 - rho));
  vector[n] a1 = (col(z,2) ./ col(z,1) - rho) ./ denom;
  vector[n] a2 = (col(z,1) ./ col(z,2) - rho) ./ denom;
  vector[n] product = col(z,1) .* col(z,2);
  matrix[n,2] z_probit = Phi_approx(z);
  real delta;
  row_vector[n] out;
  row_vector[n] raw;

  for(i in 1:n){
    if( rho[i] == 1){
      raw[i] = min( row(z_probit,i) );
    }else if(rho[i] == -1){
      raw[i] = sum( row(z_probit,i) ) - 1;
    }else if(rho[i] == 0){
      raw[i] = prod( row(z_probit,i) );
    }else if((z[i,1] == 0 && z[i,2] == 0) || (z[i,1] == rho[i]*z[i,2])){
      raw[i] = 0.25 + asin(rho[i]) / (2.0 * pi());
    }else {
      delta = product[i] < 0 || (product[i] == 0 && (z[i,1] + z[i,2]) < 0);
      raw[i] = 0.5 * (z_probit[i,1] + z_probit[i,2] - delta) - owens_t(z[i,1], a1[i]) - owens_t(z[i,2], a2[i]);
    }
    // machine precision is such that this ends up negative sometimes.
    out[i] = raw[i] < 0 ? 0 : raw[i];
  }
  return out;
}

real binormal_cdf_real(vector z, real rho) {
  real denom = sqrt((1.0 + rho) * (1.0 - rho));
  real a1 = (z[2] / z[1] - rho) / denom;
  real a2 = (z[1] / z[2] - rho) / denom;
  real product = z[1] * z[2];
  vector[2] z_probit = Phi_approx(z);
  real delta;
  real out;
  real raw;

  if( rho == 1){
    raw = min( z_probit );
  }else if(rho == -1){
    raw = sum( z_probit ) - 1;
  }else if(rho == 0){
    raw = prod( z_probit );
  }else if((z[1] == 0 && z[2] == 0) || (z[1] == rho*z[2])){
    raw = 0.25 + asin(rho) / (2.0 * pi());
  }else {
    delta = product < 0 || (product == 0 && (z[1] + z[2]) < 0);
    raw = 0.5 * (z_probit[1] + z_probit[2] - delta) - owens_t(z[1], a1) - owens_t(z[2], a2);
  }
  out = raw < 0 ? 0 : raw;
  return out;
}

real biprobit_lpdf_real(row_vector Y, real mu1, real mu2, real rho) {
  vector[2] q = 2.0 * Y' - 1.0;
  vector[2] z = q .* to_vector([mu1, mu2]);
  real rho1 = q[1] * q[2] * rho;

  return log(binormal_cdf_real(z, rho1));
}

row_vector biprobit_lpdf_vector(matrix Y, matrix mu, vector rho) {
  int n = rows(Y);
  matrix[n, 2] q = 2.0 * Y - 1.0;
  matrix[n, 2] z = q .* mu;
  vector[n] rho1 = col(q,1) .* col(q,2) .* rho;

  return log(binormal_cdf_vector(z, rho1));
}
