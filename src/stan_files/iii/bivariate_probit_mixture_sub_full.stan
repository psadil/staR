functions{
#include /functions/binormal.stan
}
data {
  int<lower=0> n; // number of observations
  int<lower=1> n_condition; // number of conditions (4)
  int<lower=1,upper=n_condition> condition[n];
  int<lower=1> n_subject; // The number of subjects
  int<lower=1,upper=n_subject> subject[n]; // Index indicating the subject for a current trial
  int<lower=1> n_item; // The number of subjects
  int<lower=1,upper=n_item> item[n]; // Index indicating the subject for a current trial
  matrix<lower = 0, upper=1>[n, 2] y;
  vector[8] priors;
  int n_orders;
  matrix<lower=0, upper=1>[n,n_condition] X[n_orders, 2];
  int<lower=0, upper = 1>prior_only;  // should the likelihood be ignored?
}
transformed data{
  int D = 2;
}
parameters{
  corr_matrix[D] condition_omega;
  matrix[n_condition-1, n_subject] zeta_raw[D, n_orders]; // -1 for intercept
  matrix<lower=0>[D, n_subject] beta_raw[n_orders];
  matrix[n_orders, n_subject] lambda_raw; // first value must be pinned for identifiability
  vector<lower=0>[n_orders] lambda_scale; // Variance of subject-level effects
  cholesky_factor_corr[n_orders] lambda_L;
  matrix<lower=0>[D, 2] random_scales;
  matrix[D, n_subject] beta_intercept[n_orders]; // Subject-level coefficients for the bivariate normal means
  cholesky_factor_corr[D] subject_L;
  matrix[D, n_item] item_mu_raw; // Subject-level coefficients for the bivariate normal means
  cholesky_factor_corr[D] item_L;
}
transformed parameters {
  matrix[n_orders, n_subject] lambda_tmp = diag_pre_multiply(lambda_scale, lambda_L)*lambda_raw;
  matrix[n_subject, n_orders] lambda_logged;
  matrix[n_item, D] item_mu = (diag_pre_multiply(random_scales[,1], item_L) * item_mu_raw)';
  vector[n] log_lik;

	for(s in 1:n_subject){
    lambda_logged[s] = log_softmax(lambda_tmp[,s])';
	}

	{
	  matrix[D,D] subject_Sigma_L = diag_pre_multiply(random_scales[,2], subject_L);
    matrix[n_condition, D] condition_mu_ordered[n_subject]; // condition effects on mean of latent
    matrix[n,D] mu;
    matrix[n_orders, n] lps;
    matrix[D, n_subject] subject_intercept;

	  for(order in 1:n_orders){
      subject_intercept = (subject_Sigma_L * beta_intercept[order]);

      for(s in 1:n_subject){
        condition_mu_ordered[s] = append_row(subject_intercept[,s]',
          append_col(beta_raw[order,1,s] * cumulative_sum( softmax(zeta_raw[1,order,,s] ) ),
          beta_raw[order,2,s] * cumulative_sum( softmax( zeta_raw[2,order,,s] ) )));
      }
      for(i in 1:n){
        mu[i] = [X[order,1,i] * condition_mu_ordered[subject[i],,1], X[order,2,i] * condition_mu_ordered[subject[i],,2]];
      }
      mu += item_mu[item];
      lps[order] = lambda_logged[subject, order]' + biprobit_lpdf_vector(y, mu, rep_vector(condition_omega[1,2], n) );
    }
    for (i in 1:n){
      if(is_inf(sum(col(lps,i))) && prior_only){
        log_lik[i] = negative_infinity();
      }else{
        log_lik[i] = log_sum_exp(col(lps,i));
      }
    }
	}
}
model {

  // priors
	for(d in 1:D){
	  for(order in 1:n_orders){
  		to_vector(zeta_raw[d,order]) ~ normal(0, priors[3]);
	  }
	}
  to_vector(random_scales) ~ gamma(priors[4], priors[5]);
  subject_L ~ lkj_corr_cholesky(priors[6]);
  item_L ~ lkj_corr_cholesky(priors[6]);
  to_vector(item_mu_raw) ~ std_normal();

  for(order in 1:n_orders){
    to_vector(beta_raw[order]) ~ normal(0, priors[2]);
  	to_vector(beta_intercept[order]) ~ std_normal();
  }
	lambda_scale ~ gamma(priors[4], priors[8]);
	lambda_L ~ lkj_corr_cholesky(priors[7]);
  to_vector(lambda_raw) ~ std_normal();

	condition_omega ~ lkj_corr(priors[7]);

  if (!prior_only) {
    target += sum(log_lik);
  }
}
generated quantities {
  matrix[n_subject, n_orders] lambda = exp(lambda_logged);
	matrix[n_orders,n_orders] lambda_S = multiply_lower_tri_self_transpose(lambda_L);
	real<lower=-1, upper=1> condition_rho = condition_omega[1,2];
}
