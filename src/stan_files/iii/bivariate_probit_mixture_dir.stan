functions{
#include /functions/binormal.stan
}
data {
  int<lower=0> n; // number of observations
  int<lower=1> n_condition; // number of conditions (4)
  int<lower=1,upper=n_condition> condition[n];
  int<lower=1> n_subject; // The number of subjects
  int<lower=1,upper=n_subject> subject[n]; // Index indicating the subject for a current trial
  int<lower=1> n_item; // The number of subjects
  int<lower=1,upper=n_item> item[n]; // Index indicating the subject for a current trial
  matrix<lower = 0, upper=1>[n, 2] y;
  vector[8] priors;
  int n_orders;
  matrix<lower=0, upper=1>[n,n_condition] X[n_orders, 2];
  int<lower=0, upper = 1>prior_only;  // should the likelihood be ignored?
}
transformed data{
  int D = 2;
  vector[n_condition-1] alpha_zeta = rep_vector(priors[3], n_condition-1);
  vector[n_orders] alpha_lambda = rep_vector(priors[8], n_orders);
}
parameters{
  corr_matrix[D] condition_omega;
  simplex[n_condition-1] zeta[D, n_orders]; // -1 for intercept
  matrix<lower=0>[D, n_orders] beta_raw;
  simplex[n_orders] lambda; // first value must be pinned for identifiability
  matrix<lower=0>[D, 2] random_scales;
  matrix[D, n_orders] beta_intercept; // Subject-level coefficients for the bivariate normal means
  cholesky_factor_corr[D] subject_L;
  matrix[D, n_item] item_mu_raw; // Subject-level coefficients for the bivariate normal means
  matrix[D, n_subject] subject_mu_raw; // Subject-level coefficients for the bivariate normal means
  cholesky_factor_corr[D] item_L;
}
transformed parameters {
  vector[n_orders] lambda_logged = log(lambda);
  matrix[n_item, D] item_mu = (diag_pre_multiply(random_scales[,1], item_L) * item_mu_raw)';
  matrix[n_subject, D] subject_mu = (diag_pre_multiply(random_scales[,2], subject_L) * subject_mu_raw)';
  vector[n] log_lik;
  matrix[n_condition, D] condition_mu_ordered[n_orders]; // condition effects on mean of latent

	{
    matrix[n_orders, n] lps;

	  for(order in 1:n_orders){
      condition_mu_ordered[order] = append_row(beta_intercept[,order]',
        append_col(beta_raw[1,order] * cumulative_sum( zeta[1,order]  ),
        beta_raw[2,order] * cumulative_sum( zeta[2, order])));

    lps[order] = lambda_logged[order] + biprobit_lpdf_vector(y,
      append_col(item_mu[item,1] + subject_mu[subject,1] + X[order,1] * col(condition_mu_ordered[order],1),
        item_mu[item,2] + subject_mu[subject,2] + X[order,2] * col(condition_mu_ordered[order],2)),
        rep_vector(condition_omega[1,2], n) );
      }

    for (i in 1:n){
      if(is_inf(sum(col(lps,i))) && prior_only){
        log_lik[i] = negative_infinity();
      }else{
        log_lik[i] = log_sum_exp(col(lps,i));
      }
    }
	}
}
model {

  // priors
	for(d in 1:D){
	  for(order in 1:n_orders){
	    for(s in 1:n_subject){
    		zeta[d,order] ~ dirichlet(alpha_zeta);
	    }
	  }
	}
  to_vector(random_scales) ~ gamma(priors[4], priors[5]);
  subject_L ~ lkj_corr_cholesky(priors[6]);
  item_L ~ lkj_corr_cholesky(priors[6]);
  to_vector(item_mu_raw) ~ std_normal();
  to_vector(subject_mu_raw) ~ std_normal();

  to_vector(beta_raw) ~ normal(0, priors[2]);
	to_vector(beta_intercept) ~ normal(0, priors[2]);

  lambda ~ dirichlet(alpha_lambda);

	condition_omega ~ lkj_corr(priors[7]);

  if (!prior_only) {
    target += sum(log_lik);
  }
}
generated quantities {
	real<lower=-1, upper=1> condition_rho = condition_omega[1,2];
}
