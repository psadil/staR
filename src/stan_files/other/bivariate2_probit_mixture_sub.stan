functions{
#include /functions/binormal.stan
}
data {
  int<lower=0> n; // number of observations
  int<lower=1> n_condition; // number of conditions (4)
  int<lower=1,upper=n_condition> condition[n];
  int<lower=1> n_subject; // The number of subjects
  int<lower=1,upper=n_subject> subject[n]; // Index indicating the subject for a current trial
  int<lower=1> n_item; // The number of subjects
  int<lower=1,upper=n_item> item[n]; // Index indicating the subject for a current trial
  matrix<lower = 0, upper=1>[n, 2] y;
  vector[8] priors;
  int n_orders;
  matrix<lower=0, upper=1>[n,n_condition] X[n_orders, 2];
  int<lower=0, upper = 1>prior_only;  // should the likelihood be ignored?
}
transformed data{
  int D = 2;
}
parameters{
  corr_matrix[D] condition_omega[n_orders, n_condition];
  matrix[n_condition-1, n_orders] zeta_raw[D]; // -1 for intercept, -1 for simplex identification
  matrix[n_orders, D] beta_intercept; // when baseline and final condition fixed, these must not be shared across orders
  matrix<lower=0>[D, n_orders] beta_raw;
  matrix[n_subject, n_orders] lambda_raw; // first value must be pinned for identifiability
  vector<lower=0>[D] subject_scale; // Variance of subject-level effects
  matrix[D, n_subject] subject_mu_raw; // Subject-level coefficients for the bivariate normal means
  cholesky_factor_corr[D] subject_L;
  vector<lower=0>[D] item_scale; // Variance of subject-level effects
  matrix[D, n_item] item_mu_raw; // Subject-level coefficients for the bivariate normal means
  cholesky_factor_corr[D] item_L;
}
transformed parameters {
  matrix[n_subject, n_orders] lambda_logged;
  matrix[n_subject, D] subject_mu = (diag_pre_multiply(subject_scale, subject_L) * subject_mu_raw)';
  matrix[n_item, D] item_mu = (diag_pre_multiply(item_scale, item_L) * item_mu_raw)';
  matrix[n_orders, n] lps;
  vector[n] log_lik;
  matrix[n_condition, D] condition_mu_ordered[n_orders]; // condition effects on mean of latent

	for(s in 1:n_subject){
    lambda_logged[s] = log_softmax(lambda_raw[s]')';
	}

  for(order in 1:n_orders){

    condition_mu_ordered[order] = append_row(beta_intercept[order],
      append_col(beta_raw[1, order] * cumulative_sum( softmax(zeta_raw[1,, order] ) ),
        beta_raw[2, order] * cumulative_sum( softmax( zeta_raw[2,, order] ) )));

    lps[order] = to_row_vector(lambda_logged[subject, order]) + biprobit_lpdf_vector(y,
      subject_mu[subject] + item_mu[item] +
      append_col(X[order,1] * col(condition_mu_ordered[order], 1), X[order,2] * col(condition_mu_ordered[order], 2)),
      to_vector(condition_omega[order, condition,1,2]) );
  }
  for (i in 1:n){
    if(is_inf(sum(col(lps,i))) && prior_only){
      log_lik[i] = negative_infinity();
    }else{
      log_lik[i] = log_sum_exp(col(lps,i));
    }
  }
}
model {

  // priors
	for(d in 1:D){
		to_vector(zeta_raw[d]) ~ normal(0, priors[3]);
	}
	to_vector(beta_intercept) ~ normal(0, priors[2]);
	to_vector(beta_raw) ~ normal(0, priors[2]);
  subject_scale ~ gamma(priors[4], priors[5]);
  subject_L ~ lkj_corr_cholesky(priors[6]);
  to_vector(subject_mu_raw) ~ std_normal();
  item_scale ~ gamma(priors[4], priors[5]);
  item_L ~ lkj_corr_cholesky(priors[6]);
  to_vector(item_mu_raw) ~ std_normal();

  for(cond in 1:n_condition){
    for(order in 1:n_orders){
  		condition_omega[order, cond] ~ lkj_corr(priors[7]);
    }
	}

  to_vector(lambda_raw) ~ normal(0, priors[8]);

  if (!prior_only) {
    target += sum(log_lik);
  }
}
generated quantities {
  matrix[n_subject, n_orders] lambda = exp(lambda_logged);
  real<lower=-1.0, upper=1.0> subject_rho; // correlation to assemble
  real<lower=-1.0, upper=1.0> item_rho; // correlation to assemble
	matrix<lower=-1, upper=1>[n_orders, n_condition] condition_rho;

	for(cond in 1:n_condition){
	  for(order in 1:n_orders){
  		condition_rho[order, cond] = condition_omega[order, cond,1,2]; // correlation to assemble
	  }
	}
  {
    matrix[D, D] Sigma;
    Sigma = multiply_lower_tri_self_transpose(subject_L);
    subject_rho = Sigma[1,2];
    Sigma = multiply_lower_tri_self_transpose(item_L);
    item_rho = Sigma[1,2];

  }
}
