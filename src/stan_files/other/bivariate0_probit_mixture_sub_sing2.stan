functions{
#include /functions/binormal.stan
}
data {
  int<lower=0> n; // number of observations
  int<lower=1> n_condition; // number of conditions (4)
  int<lower=1,upper=n_condition> condition[n];
  int<lower=1> n_subject; // The number of subjects
  int<lower=1,upper=n_subject> subject[n]; // Index indicating the subject for a current trial
  int<lower=1> n_item; // The number of subjects
  int<lower=1,upper=n_item> item[n]; // Index indicating the subject for a current trial
  matrix<lower = 0, upper=1>[n, 2] y;
  vector[8] priors;
  int n_orders;
  matrix<lower=0, upper=1>[n,n_condition] X[n_orders, 2];
  int<lower=0, upper = 1>prior_only;  // should the likelihood be ignored?
}
transformed data{
  int D = 2;
}
parameters{
  corr_matrix[D] condition_omega;
  matrix[n_condition-1, n_orders] zeta_raw[D]; // -1 for intercept
  matrix[n_orders, D] beta_intercept; // when baseline and final condition fixed, these must not be shared across orders
  matrix<lower=0>[D, n_orders] beta_raw;
  matrix[n_orders, n_subject] lambda_raw; // first value must be pinned for identifiability
  vector<lower=0>[n_orders] lambda_scale; // Variance of subject-level effects
  cholesky_factor_corr[n_orders] lambda_L;
  matrix<lower=0>[D, 2] random_scales;
  matrix[D, n_subject] subject_mu_raw; // Subject-level coefficients for the bivariate normal means
  cholesky_factor_corr[D] subject_L;
  matrix[D, n_item] item_mu_raw; // Subject-level coefficients for the bivariate normal means
  cholesky_factor_corr[D] item_L;
}
transformed parameters {
  matrix[n_subject, D] subject_mu = (diag_pre_multiply(random_scales[,1], subject_L) * subject_mu_raw)';
  matrix[n_item, D] item_mu = (diag_pre_multiply(random_scales[,2], item_L) * item_mu_raw)';
  matrix[n_subject, n_orders] lambda_logged;
  matrix[n_orders, n] lps;
  vector[n] log_lik;
  matrix[n_condition, D] condition_mu_ordered[n_orders]; // condition effects on mean of latent
  matrix[n_orders, n_subject] lambda_tmp = diag_pre_multiply(lambda_scale, lambda_L)*lambda_raw;

	for(s in 1:n_subject){
    lambda_logged[s] = log_softmax(lambda_tmp[,s])';
	}

  for(order in 1:n_orders){

    condition_mu_ordered[order] = append_row(beta_intercept[order],
      append_col(beta_raw[1, order] * cumulative_sum( softmax(zeta_raw[1,, order] ) ),
        beta_raw[2, order] * cumulative_sum( softmax( zeta_raw[2,, order] ) )));

    lps[order] = to_row_vector(lambda_logged[subject, order]) + biprobit_lpdf_vector(y,
      subject_mu[subject] + item_mu[item] +
      append_col(X[order,1] * col(condition_mu_ordered[order], 1), X[order,2] * col(condition_mu_ordered[order], 2)),
      rep_vector(condition_omega[1,2], n) );
  }
  for (i in 1:n){
    if(is_inf(sum(col(lps,i))) && prior_only){
      log_lik[i] = negative_infinity();
    }else{
      log_lik[i] = log_sum_exp(col(lps,i));
    }
  }
}
model {

  // priors
	for(d in 1:D){
		to_vector(zeta_raw[d]) ~ std_normal();
	}
	to_vector(beta_intercept) ~ normal(0, priors[2]);
	to_vector(beta_raw) ~ normal(0, priors[2]);
  to_vector(random_scales) ~ gamma(priors[4], priors[5]);
  subject_L ~ lkj_corr_cholesky(priors[6]);
  to_vector(subject_mu_raw) ~ std_normal();
  item_L ~ lkj_corr_cholesky(priors[6]);
  to_vector(item_mu_raw) ~ std_normal();

	condition_omega ~ lkj_corr(priors[7]);
	lambda_scale ~ gamma(priors[4], priors[8]);
	lambda_L ~ lkj_corr_cholesky(priors[7]);
  to_vector(lambda_raw) ~ std_normal();

  if (!prior_only) {
    target += sum(log_lik);
  }
}
generated quantities {
  matrix[n_subject, n_orders] lambda = exp(lambda_logged);
  real<lower=-1.0, upper=1.0> subject_rho; // correlation to assemble
  real<lower=-1.0, upper=1.0> item_rho; // correlation to assemble
	real<lower=-1, upper=1> condition_rho = condition_omega[1,2];
	matrix[n_orders,n_orders] lambda_S = multiply_lower_tri_self_transpose(lambda_L);

  {
    matrix[D, D] Sigma;
    Sigma = multiply_lower_tri_self_transpose(subject_L);
    subject_rho = Sigma[1,2];
    Sigma = multiply_lower_tri_self_transpose(item_L);
    item_rho = Sigma[1,2];

  }
}
