functions{
#include /functions/binormal.stan
}
data {
  int<lower=0> n; // number of observations
  int<lower=1> n_condition; // number of conditions (4)
  int<lower=1,upper=n_condition> condition[n];
  int<lower=1> n_subject; // The number of subjects
  int<lower=1,upper=n_subject> subject[n]; // Index indicating the subject for a current trial
  int<lower=1> n_item; // The number of subjects
  int<lower=1,upper=n_item> item[n]; // Index indicating the subject for a current trial
  matrix<lower = 0, upper=1>[n, 2] y;
  vector[8] priors;
  int n_orders;
  matrix<lower=0, upper=1>[n,n_condition] X[n_orders, 2];
  int<lower=0, upper = 1>prior_only;  // should the likelihood be ignored?
}
transformed data{
  int D = 2;
  row_vector[n_orders] zeroes_order = rep_row_vector(0,n_orders);
  row_vector[n_subject] zeroes_sub = rep_row_vector(0, n_subject);
  vector[n_orders] alpha_lambda = rep_vector(priors[8], n_orders);
  vector[n_condition-1] alpha_zeta = rep_vector(priors[3], n_condition-1);
}
parameters{
  simplex[n_condition-1] zeta[D, n_orders]; // -1 for intercept
  simplex[n_orders] lambda[n_subject]; // first value must be pinned for identifiability
  matrix[n_orders, D] beta_intercept; // when baseline and final condition fixed, these must not be shared across orders
  matrix<lower=0>[D, n_orders] beta_raw;
  vector<lower=0>[D] subject_scale; // Variance of subject-level effects
  matrix[D, n_subject] subject_mu_raw; // Subject-level coefficients for the bivariate normal means
  vector<lower=0>[D] item_scale; // Variance of subject-level effects
  matrix[D, n_item] item_mu_raw; // Subject-level coefficients for the bivariate normal means
}
transformed parameters {
  matrix[n_subject, D] subject_mu = (diag_matrix(subject_scale) * subject_mu_raw)';
  matrix[n_item, D] item_mu = (diag_matrix(item_scale) * item_mu_raw)';
  vector[n_subject] log_lik;
  matrix[n_condition, D] condition_mu_ordered[n_orders]; // condition effects on mean of latent

  {
    matrix[n_orders, n_subject] lambda_logged;
		 for(s in 1:n_subject){
		   lambda_logged[,s] = log(lambda[s]);
		 }

		 for(order in 1:n_orders){
		 condition_mu_ordered[order] = append_row(beta_intercept[order],
      append_col(beta_raw[1, order] * cumulative_sum( zeta[1, order] ),
        beta_raw[2, order] * cumulative_sum( zeta[2, order] ) ));
		 }

    for (s in 1:n_subject){
      vector[n_orders] lps = lambda_logged[,s];
      for(order in 1:n_orders){
      vector[n_item] lps_tmp;
      int trial = 0;
      for(i in 1:n){
        if(subject[i] == s){
          trial += 1;
          lps_tmp[trial] = biprobit_lpdf_real(y[i],
            subject_mu[subject[i],1] + item_mu[item[i],1] + X[order,1,i] * condition_mu_ordered[order,,1],
            subject_mu[subject[i],2] + item_mu[item[i],2] + X[order,2,i] * condition_mu_ordered[order,,2],
            0);
        }
      }
        lps[order] += sum(lps_tmp);
      }
      if(is_inf(sum(lps)) && prior_only){
        log_lik[s] = negative_infinity();
      }else{
        log_lik[s] = log_sum_exp(lps);
      }
    }
  }
}
model {

  // priors
  for(order in 1:n_orders){
    for(d in 1:D){
      zeta[d, order] ~ dirichlet(alpha_zeta);
	  }
	}
	to_vector(beta_intercept) ~ normal(0, priors[2]);
	to_vector(beta_raw) ~ normal(0, priors[2]);
  subject_scale ~ gamma(priors[4], priors[5]);
  to_vector(subject_mu_raw) ~ std_normal();
  item_scale ~ gamma(priors[4], priors[5]);
  to_vector(item_mu_raw) ~ std_normal();

	for(s in 1:n_subject){
	  lambda[s] ~ dirichlet(alpha_lambda);
	}

  if (!prior_only) {
    target += sum(log_lik);
  }
}
