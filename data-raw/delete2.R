
tidybayes::spread_draws(stanfit_target_standata_replication_FALSE_bivariate_probit_unordered, beta[condition, S]) %>%
  ungroup() %>%
  mutate(S = if_else(S==1, "afc","name"),
         condition = factor(condition)) %>%
  spread(S, beta) %>%
  ggplot(aes(x=afc, y = name, color=condition)) +
  coord_fixed() +
  stat_ellipse(geom="polygon", alpha=.3) +
  geom_point(alpha=.1)+
  scale_color_viridis_d(option="inferno") +
  ggsave("data-raw/post-betas_unordered_replication.png")

d <- lateral %>%
  filter(expt == "original") %>%
  dplyr::mutate(condition = as.numeric(condition),
                condition = factor(condition)) %>%
  dplyr::distinct(condition) %>%
  dplyr::mutate(dimension = case_when(condition == 1 ~ "study",
                                      condition == 2 ~ "study",
                                      condition == 3 ~ "study",
                                      condition == 4 ~ "study"),
                trace = case_when(condition == 1 ~ 0,
                                  condition == 2 ~ 1,
                                  condition == 3 ~ 1,
                                  condition == 4 ~ 2)) %>%
  dplyr::rename(study_code = condition) %>%
  dplyr::mutate(study_code = as.integer(study_code),
                dimension = factor(dimension, levels = c("study"), ordered = TRUE),
                trace = factor(trace, ordered = TRUE))

mono.trace <- make_trace(d, trace = TRUE, state = TRUE, monotonic = TRUE)
nonmono.trace <- make_trace(d, trace = TRUE, state = TRUE, monotonic = FALSE)

perms <- gtools::permutations(8, 8)

#### ---------------------------------
drake::loadd(stanfit_target_standata_replication_FALSE_bivariate_probit_unordered)
drake::loadd(stanfit_target_standata_replication_TRUE_bivariate_probit_unordered)

post_Sigma <- tidybayes::spread_draws(stanfit_target_standata_replication_FALSE_bivariate_probit_unordered, beta[condition, S]) %>%
  ungroup() %>%
  mutate(S = if_else(S==1, "afc","name")) %>%
  unite(col = condition_state, S, condition) %>%
  spread(condition_state, beta) %>%
  select(matches(".*[[:digit:]]")) %>%
  as.matrix()

# should be pretty close to 1.
post.probs <- apply(perms, 1,
                    post.prob.order,
                    mus = colMeans(post_Sigma),
                    covs = stats::cov(post_Sigma),
                    lower = NULL,
                    upper = NULL)
post.probs <- post.probs / sum(post.probs)

prior_Sigma <- tidybayes::spread_draws(stanfit_target_standata_replication_TRUE_bivariate_probit_unordered, beta[condition, S]) %>%
  ungroup() %>%
  mutate(S = if_else(S==1, "afc","name")) %>%
  unite(col = condition_state, S, condition) %>%
  spread(condition_state, beta) %>%
  select(matches(".*[[:digit:]]")) %>%
  as.matrix()

# should be pretty close to 1.
prior.probs <- apply(perms, 1,
                     post.prob.order,
                     mus = colMeans(prior_Sigma),
                     covs = stats::cov(prior_Sigma),
                     lower = NULL,
                     upper = NULL)
prior.probs <- prior.probs / sum(prior.probs)


mono_orders <- vector(length = nrow(perms))
for(o in 1:nrow(mono.trace)){
  mono_orders <- mono_orders | (rowSums(perms == mono.trace[o, col(perms)]) == ncol(perms) )
}

nonmono_orders <- vector(length = nrow(perms))
for(o in 1:nrow(nonmono.trace)){
  nonmono_orders <- nonmono_orders | (rowSums(perms == nonmono.trace[o,col(perms)]) == ncol(perms) )
}

nonmono.post <- sum(post.probs[nonmono_orders])
mono.post <- sum(post.probs[mono_orders])

nonmono.prior <- sum(prior.probs[nonmono_orders])
mono.prior <- sum(prior.probs[mono_orders])

bf.mono.to.enc <- (1/mono.prior)/(1/mono.post)
bf.nonmono.to.enc <- (1/nonmono.prior)/(1/nonmono.post)

bf.nonmono.to.mono <- bf.nonmono.to.enc / bf.mono.to.enc
# 1.58 in support of non-monotonicity
# only 5.75 in favor of non-monotonicity. Still, that's > 1!

