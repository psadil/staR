data {
  int<lower=0> n;
  vector[2] priors;
}
parameters{
  ordered[n] mu;
}
model {

  mu ~ std_normal();

}
