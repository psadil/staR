Analysis of working memory experiment using state-trace
========================================================



```{r,echo=FALSE}

rm(list=ls())
devtools::load_all()
library(tidyverse)
# Data frame containing raw data, must be called "data"
load("WMdata.RData") 
# In this example data 0.1% non-resposnes and 
#                      2.0% of responses < .2s and >3s removed
# MAP FACTORS TO STANDARD NAMES (!! CHANGE THIS FOR YOUR DESIGN !!)

names(data)[names(data)=="subjNumber"] <- "s"  # subjects
names(data)[names(data)=="setSize"]    <- "T"  # Trace
names(data)[names(data)=="silent"]     <- "D"  # Dimension
names(data)[names(data)=="sequential"] <- "S"  # State
names(data)[names(data)=="change"]     <- "R"  # Response
names(data)[names(data)=="CResp"]      <- "C"  # Response score
data <- data[,c("s","T","D","S","R","C")]

# CHECK DATA IS IN THE RIGHT FORM

if (!is.logical(data$C)) 
  stop("Response score must be a logical")
levs <- lapply(data[,c("s","T","D","S","R")],levels)
not.fac <- unlist(lapply(levs,is.null))
if (any(not.fac)) 
  stop(paste("These columns are not factors:",
             paste(names(not.fac)[not.fac],collapse=" ")))
if (!(length(levs$S)==2)) 
  stop("State factor must have 2 levels")

cat(paste(length(levels(data$s)),"SUBJECTS with DESIGN LEVELS\n\n"))
lapply(data[,c("T","D","S")],levels)

# COUNT NUMBER OF CORRECT RESPONSES (M) AND TRIALS (N)

corrects = tapply(data$C,list(s=data$s,T=data$T,D=data$D,S=data$S,R=data$R),sum)
Ntotal = table(s=data$s,T=data$T,D=data$D,S=data$S,R=data$R)
correctsDF = as.data.frame.table(corrects)
NtotalDF = as.data.frame.table(Ntotal)
if(  all(correctsDF[,1:5] == NtotalDF[,1:5]) ) { # Check factors are line up
  correctsDF$N = NtotalDF$Freq
  colnames(correctsDF)[6] = "M"
} else {
  stop("Could not merge data sets.")
}


# GET ACCURACY MEAN AND STANDARD ERROR ASSUMING A UNIFORM PRIOR

correctsDF$phat = (correctsDF$M + 1) / (correctsDF$N + 2)
correctsDF$stdErr = sqrt(correctsDF$phat * 
                  (1 - correctsDF$phat) / (correctsDF$N + 2))


# COMBINE DATA OVER TWO RESPONSE TYPES WIGHTED BY NUMBER OF TRIALS

chg = correctsDF[correctsDF$R=="change",]
sme = correctsDF[correctsDF$R=="same",]
if(  all(sme[,1:4] == chg[,1:4]) ){ # Check factors line up
  combinedDat = sme[,1:4] # subject and T, D, S factors
  # weighted average
  combinedDat$phat = (sme$N * sme$phat + chg$N * chg$phat) / (sme$N + chg$N)
  combinedDat$stdErr = sqrt( sme$N^2 * sme$stdErr^2 + chg$N^2 * chg$stdErr^2 ) / 
    (sme$N + chg$N)
  smePrec = 1/(sme$stdErr/dnorm(qnorm(sme$phat)))^2
  chgPrec = 1/(chg$stdErr/dnorm(qnorm(chg$phat)))^2

  combinedDat$probitMean = (smePrec * qnorm(sme$phat) + chgPrec * qnorm(chg$phat)) / 
    (smePrec + chgPrec)
  combinedDat$probitStdErr = sqrt(1 / (smePrec + chgPrec))
   
  
#  combinedDat$probitMean = (sme$N * qnorm(sme$phat) + chg$N * qnorm(chg$phat)) / 
#    (sme$N + chg$N)
#  combinedDat$probitStdErr = sqrt(sme$N^2 * (sme$stdErr/dnorm(qnorm(sme$phat)))^2 + 
#                                  chg$N^2 * (chg$stdErr/dnorm(qnorm(chg$phat)))^2) / 
#    (sme$N + chg$N)  

} else {
  stop("Could not merge same and change.")
}

cat("AVERAGE ACCURACY\n\n")
round(tapply(combinedDat$phat,combinedDat[,c("T","D","S")],mean),2)

ppp.probit.lower0 <- getPPP(dat=combinedDat,
                     trace.increasing=FALSE,
                     D.order=c("articulate","silent"),
                     lower=0)
bfs.p0 <- getBF(ppp.probit.lower0)


```

There's a huge question in my mind below about there the calculations come from. The estimation of phat makes sense, that's just the posterior distribution assuming a beta(1,1) prior on each parameter, what Wikipedia refers to as the [Bayes' prior](https://en.wikipedia.org/wiki/Beta_distribution#Bayesian_inference). Though, it seems like they used that phat value to estimate the standardized error of a binomial distribution. I think what they want instead is the variance of the phat value.

```{r,echo=FALSE}

data("lateral", package="datalateral")
original <- lateral %>%
  filter(expt == "original" & !(list=="0")) %>%
  dplyr::mutate(condition = as.numeric(condition),
                condition = factor(condition)) %>%
  dplyr::mutate(D = case_when(condition == 1 ~ "no",
                              condition == 2 ~ "word",
                              condition == 3 ~ "image",
                              condition == 4 ~ "image"),
                D = factor(D, levels = c("no", "word", "image")),
                `T` = case_when(condition == 1 ~ 0,
                                condition == 2 ~ 1,
                                condition == 3 ~ 1,
                                condition == 4 ~ 2),
                `T` = factor(`T`)) %>%
  gather(S, C, c("afc","name")) %>%
  mutate(S = factor(S)) %>%
  rename(s="subject") %>%
  select(s, `T`, D, S, C) %>%
  group_by(s, `T`, D, S) %>%
  summarise(phat = (sum(C)+1)/(n() + 2), # Bayes posterior: https://en.wikipedia.org/wiki/Beta_distribution#Bayesian_inference
            stdErr = sqrt(phat*(1-phat)/(n()+2)),
            # stdErr = sqrt( ((n()+sum(C)+1)*(sum(C)+1))/((3+n())*(2+n())^2) ),
            probitMean = qnorm(phat),
            probitStdErr = stdErr/dnorm(qnorm(phat))) %>%
  ungroup() %>%
  mutate(s = forcats::fct_drop(s)) 

# wm.av.p <- tapply(combinedDat$phat,combinedDat[,c("T","D","S")],mean)
# save(wm.av.p,file="wm_av_p.RData")

# PERFORM STATE TRACE ANALYSIS

source('StateTraceLaplace.R')

# CALCUALTE PRIOR AND POSTERIOR PROBABILITIES AND BAYES FACTORS


# PREFERED ANALYSIS: PROBIT SCALE ABOVE CHANCE

cat("CALCULATING BAYES FACTORS\n\n")


ppp.probit <- getPPP(dat = original,
                     trace.increasing=TRUE,
                     lower = NULL,
                     upper = NULL)

BF0.m.nm <- apply(ppp.probit$post/ppp.probit$prior,1,function(x){x[2]/x[1]})

BF.trace <- data.frame(ppp.probit$post.trace/ppp.probit$prior.trace)
BF1.m.nm <- apply(BF.trace,1,function(x){x["mono"]/x["nonmono"]})


replication <- lateral %>%
  filter(expt == "replication" & !(list=="0")) %>%
  dplyr::mutate(condition = as.numeric(condition),
                condition = factor(condition)) %>%
  dplyr::mutate(D = case_when(condition == 1 ~ "no",
                              condition == 2 ~ "word",
                              condition == 3 ~ "image",
                              condition == 4 ~ "image"),
                D = factor(D, levels = c("no", "word", "image")),
                `T` = case_when(condition == 1 ~ 0,
                                condition == 2 ~ 1,
                                condition == 3 ~ 1,
                                condition == 4 ~ 2),
                `T` = factor(`T`)) %>%
  gather(S, C, c("afc","name")) %>%
  mutate(S = factor(S)) %>%
  rename(s="subject") %>%
  select(s, `T`, D, S, C) %>%
  group_by(s, `T`, D, S) %>%
  summarise(phat = (sum(C)+1)/(n() + 2), # Bayes posterior: https://en.wikipedia.org/wiki/Beta_distribution#Bayesian_inference
            stdErr = sqrt(phat*(1-phat)/(n()+2)),
            # stdErr = sqrt( ((n()+sum(C)+1)*(sum(C)+1))/((3+n())*(2+n())^2) ),
            probitMean = qnorm(phat),
            probitStdErr = stdErr/dnorm(qnorm(phat))) %>%
  ungroup() %>%
  mutate(s = forcats::fct_drop(s)) 

tibble(subject = factor(as.numeric(names(BF1.m.nm))),
       BF = BF1.m.nm) %>%
  ggplot(aes(x=subject, y=BF)) +
  geom_point()

ppp.probit.replication <- getPPP(dat = replication,
                                 trace.increasing=TRUE,
                                 lower = NULL,
                                 upper = NULL)

BF0.m.nm.replication <- apply(ppp.probit.replication$post/ppp.probit.replication$prior,1,function(x){x[2]/x[1]})

BF.trace.replication <- data.frame(ppp.probit.replication$post.trace/ppp.probit.replication$prior.trace)
BF1.m.nm.replication <- apply(BF.trace.replication,1,function(x){x["mono"]/x["nonmono"]})


tibble(subject = factor(as.numeric(c(names(BF1.m.nm.replication), names(BF1.m.nm)))),
       BF = 1/c(BF1.m.nm.replication, BF1.m.nm),
       study =c(rep("replication",times = length(BF1.m.nm.replication)), rep("original", times=length(BF1.m.nm)))) %>%
  ggplot(aes(x=subject, y=BF)) +
  facet_wrap(~study, scales="free_x") +
  geom_point() +
  scale_y_continuous(name = "BF: nonmonotonic/monotonic") + 
  theme(axis.text.x = element_blank(),
        axis.ticks.x = element_blank()) +
  ggsave("data-raw/BF-subject.png")

```


```{r fig.width=9, fig.height=7, echo=FALSE}
# COMAPRE BAYES FACTORS, FIGURE 3 IN PAPER

plot.order <- order(bfs$BF1$m.nm)
nams <- dimnames(ppp$prior)[[1]] # Original names
nams <- nams[plot.order]
names(nams) <- letters[1:15]
nams
cex=1.5
par(mar= c(5, 5, 4, 2) + 0.1)
plot(1:15,bfs$BF1$m.nm[plot.order],ylim=c(-0.5,6.9),pch=1,cex=cex,
     xaxt="n",xlab="Participants",ylab=expression(paste(log[10],"(BF)")))
axis(1,1:15,letters[1:15])
lines(1:15,bfs$BF1$m.nm[plot.order])
points(1:15,bfs$BF0$m.nm[plot.order],pch=2,cex=cex)
lines(1:15,bfs$BF0$m.nm[plot.order])
points(1:15,bfs$BF1$t.nt[plot.order],pch=8,cex=cex)
lines(1:15,bfs$BF1$t.nt[plot.order])
abline(h=0)          # > = monotonic
abline(h=log10(3),lty=3)   # > = positive
abline(h=log10(20),lty=3)  # > = strong
abline(h=log10(150),lty=3) # > = very strong
points(1:15,bfs$BF2$m.nm[plot.order],pch=16,cex=cex)
lines(1:15,bfs$BF2$m.nm[plot.order])
legend("top",c("T vs. NT","M vs. NM","M vs. NM | T","M vs. NM | T & D"),
  pch=c(8,2,1,16),lty=1,horiz=T,bty="n",cex=.7)
par(mar= c(5, 4, 4, 2) + 0.1)
```



```{r fig.width=9, fig.height=7, echo=FALSE}
# PLOT TWO SELECTED SUBJECTS, FIGURE 2 IN PAPER
plot.order <- order(bfs$BF1$m.nm)
nams <- dimnames(ppp$prior)[[1]] # Original names
nams <- nams[plot.order]
names(nams) <- letters[1:15]
par(mfrow=c(2,3))
cex=1.5; cex1=1; cex2=1
plot.st(dat=combinedDat[combinedDat$s==nams["a"],],
        main="Participant a \n Unrestricted",cex=cex,
        probit=TRUE,ylim=c(.5,1),xlim=c(.5,1),
        trace.increasing=NULL,D.order=NULL,lower=0,
        xlab="Sequential",ylab="Simultaneous")
legend("top",legend=c("Articulate","Silent"),lty=1:2,bty="n",cex=cex1)
BFS <- c(BF=round(as.numeric(10^bfs$BF0$m.nm[nams["a"]]),1))
legend("bottom",paste(names(BFS),round(BFS,1),sep=" = "),bty="n",cex=cex2)
plot.st(dat=combinedDat[combinedDat$s==nams["a"],],
        main="Participant a \n  Trace",cex=cex,
        probit=TRUE,ylim=c(.5,1),xlim=c(.5,1),
        trace.increasing=FALSE,D.order=NULL,lower=0,
        xlab="Sequential",ylab="Simultaneous")
legend("top",legend=c("Articulate","Silent"),lty=1:2,bty="n",cex=cex1)
BFS <- c(BF=round(as.numeric(10^bfs$BF1$m.nm[nams["a"]]),1))
legend("bottom",paste(names(BFS),round(BFS,1),sep=" = "),bty="n",cex=cex2)
plot.st(dat=combinedDat[combinedDat$s==nams["a"],],
        main="Participant a \n Trace + Dimension",cex=cex,
        probit=TRUE,ylim=c(.5,1),xlim=c(.5,1),
        trace.increasing=FALSE,D.order=c("articulate","silent"),lower=0,
        xlab="Sequential",ylab="Simultaneous")
legend("top",legend=c("Articulate","Silent"),lty=1:2,bty="n",cex=cex1)
BFS=c(BF=round(as.numeric(10^bfs$BF2$m.nm[nams["a"]]),1))
legend("bottom",paste(names(BFS),round(BFS,1),sep=" = "),bty="n",cex=cex2)
plot.st(dat=combinedDat[combinedDat$s==nams["o"],],
        main="Participant o \n Unrestricted",cex=cex,
        probit=TRUE,ylim=c(.5,1),xlim=c(.5,1),
        trace.increasing=NULL,D.order=NULL,lower=0,
        xlab="Sequential",ylab="Simultaneous")
legend("top",legend=c("Articulate","Silent"),lty=1:2,bty="n",cex=cex1)
BFS <- c(BF=round(as.numeric(10^bfs$BF0$m.nm[nams["o"]]),1))
legend("bottom",paste(names(BFS),round(BFS,1),sep=" = "),bty="n",cex=cex2)
plot.st(dat=combinedDat[combinedDat$s==nams["o"],],
        main="Participant o \n  Trace",cex=cex,
        probit=TRUE,ylim=c(.5,1),xlim=c(.5,1),
        trace.increasing=FALSE,D.order=NULL,lower=0,
        xlab="Sequential",ylab="Simultaneous")
legend("top",legend=c("Articulate","Silent"),lty=1:2,bty="n",cex=cex1)
BFS <- c(BF=round(as.numeric(10^bfs$BF1$m.nm[nams["o"]]),1))
legend("bottom",paste(names(BFS),round(BFS,1),sep=" = "),bty="n",cex=cex2)
plot.st(dat=combinedDat[combinedDat$s==nams["o"],],
        main="Participant o \n Trace + Dimension",cex=cex,
        probit=TRUE,ylim=c(.5,1),xlim=c(.5,1),
        trace.increasing=FALSE,D.order=c("articulate","silent"),lower=0,
        xlab="Sequential",ylab="Simultaneous")
legend("top",legend=c("Articulate","Silent"),lty=1:2,bty="n",cex=cex1)
BFS=c(BF=round(as.numeric(10^bfs$BF2$m.nm[nams["o"]]),1))
legend("bottom",paste(names(BFS),round(BFS,1),sep=" = "),bty="n",cex=cex2)
```

