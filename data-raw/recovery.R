

devtools::load_all()
library(tidyverse)
library(drake)


wf_data <- drake::drake_plan(data = gen_dataset_cart(n_item = 40,
                                                     n_subject = 50,
                                                     condition_rho = CONDITION__RHO,
                                                     subject_rho = SUBJECT_RHO,
                                                     type = "TYPE"),
                             strings_in_dots = "literals") %>%
  drake::evaluate_plan(., rules = list(CONDITION__RHO = c(0),
                                       SUBJECT_RHO = c(0),
                                       TYPE = c("monotonic","nonmonotonic")
  ))

wf_standata <- drake::drake_plan(standata = make_standata_simul(d=dataset__, model_type = "MODEL_TYPE"),
                                 strings_in_dots = "literals") %>%
  drake::evaluate_plan(., rules = list(dataset__ = wf_data$target,
                                       MODEL_TYPE = c("monotonic","nonmonotonic")
  ))

wf_runstan <- drake::drake_plan(standata = runstan_simul(dataset__)) %>%
  drake::plan_analyses(dataset = wf_standata)


wf_plan <- rbind(wf_data, wf_standata, wf_runstan)
con <- drake::drake_config(wf_plan)
make(wf_plan)

