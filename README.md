# staR

collection of a few methods for conducting state-trace analysis in R

## Install

Package rPorta is listed as a dependency for the use of analyses presented by Davis-Stober, C. P., Morey, R. D., Gretton, M., & Heathcote, A. (2016). Bayes factors for state-trace analysis. Journal of Mathematical Psychology, 72, 116–129. https://doi.org/10.1016/j.jmp.2015.08.004 (see files abf.R, example_ABF_analysis, and statetracelaplace.R). rPorta is no longer on cran but can be installed with 

```R
install.packages("https://cran.r-project.org/src/contrib/Archive/rPorta/rPorta_0.1-9.tar.gz", repo=NULL, type="source")
```

This one can then be grabbed with
```R
remotes::install_gitlab("psadil/staR")
```
## Other packages

 - Dunn, J. C. & Kalish, M. L. (2018). State-Trace Analysis. Springer. -- [stacmr](https://github.com/michaelkalish/STA)

 - Davis-Stober, C. P., Morey, R. D., Gretton, M., & Heathcote, A. (2016). Bayes factors for state-trace analysis. Journal of Mathematical Psychology, 72, 116–129. https://doi.org/10.1016/j.jmp.2015.08.004
